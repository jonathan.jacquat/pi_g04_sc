import React from 'react';
import { Row, Form, Button, Alert } from 'react-bootstrap';
import { useNavigate, useLocation } from 'react-router';

class EnrollmentFormComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			e1: '',
			e2: '',
			e3: '',
			e4: '',
			addPerson: [{}],
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	addEnrollmentForm = () => {
		this.setState({
			addPerson: [...this.state.addPerson, {}],
		});
	};

	handleChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	}

	handleSubmit() {
		let emailList = [];
		if (this.state.e1 !== '') {
			emailList.push(this.state.e1);
			if (this.state.e2 !== '') {
				emailList.push(this.state.e2);
				if (this.state.e3 !== '') {
					emailList.push(this.state.e3);
					if (this.state.e4 !== '') {
						emailList.push(this.state.e4);
					}
				}
			}
		}
		this.props.handleEnroll(emailList);
	}

	render() {
		return (
			<Form>
				{this.props.showEnrollError && <Alert variant='danger'>Nombre d'inscrits dépasse la limite d'inscription</Alert>}
				<Row>
					<Form.Group className='mb-3' controlId='formBasicEmail'>
						<Form.Label>Persone 1</Form.Label>
						<Form.Control type='email' placeholder='exemple@gmail.com' name='e1' onChange={this.handleChange} />
					</Form.Group>
				</Row>
				<Row>
					<Form.Group className='mb-3' controlId='formBasicEmail'>
						<Form.Label>Persone 2</Form.Label>
						<Form.Control type='email' placeholder='exemple@gmail.com' name='e2' onChange={this.handleChange} />
					</Form.Group>
				</Row>
				<Row>
					<Form.Group className='mb-3' controlId='formBasicEmail'>
						<Form.Label>Persone 3</Form.Label>
						<Form.Control type='email' placeholder='exemple@gmail.com' name='e3' onChange={this.handleChange} />
					</Form.Group>
				</Row>
				<Row>
					<Form.Group className='mb-3' controlId='formBasicEmail'>
						<Form.Label>Persone 4</Form.Label>
						<Form.Control type='email' placeholder='exemple@gmail.com' name='e4' onChange={this.handleChange} />
					</Form.Group>
				</Row>
				<Row>
					<Form.Group className='mb-3'>
						<Button variant='outline-success' onClick={this.handleSubmit}>
							Confirmer
						</Button>
					</Form.Group>
				</Row>
			</Form>
		);
	}
}

function EnrollmentForm(props) {
	const { state } = useLocation();
	let navigate = useNavigate();

	return <EnrollmentFormComponent {...props} location={state} navigate={navigate} />;
}

export default EnrollmentForm;
