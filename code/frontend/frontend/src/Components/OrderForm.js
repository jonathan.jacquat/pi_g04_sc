import React from 'react';
import { Row, Col, Form, Button, Modal } from 'react-bootstrap';
import Purgatory from './Purgatory';

class OrderForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
    this.handlePurgatory = this.handlePurgatory.bind(this);
  }

  handleModal() {
    this.setState({ show: !this.state.show });
  }

  handlePurgatory() {
    this.setState({ show: false });
  }

  render() {
    const userConnected = this.props.userConnected;
    const userRole = this.props.userRole;
    return (
      <div>
        {(!userConnected || (userConnected && userRole === 'client')) && (
          <Form.Group as={Row} className='mb-2 mt-3 fs-5'>
            <Form.Label column xs='3' lg='2' xxl='1' className='mb-2'>
              Trier par :
            </Form.Label>
            <Col xs='9' lg='10' xxl='11'>
              <Form.Group controlId='formBasicSelect'>
                <Form.Control as='select' onChange={(e) => this.props.sortedBy(e)}>
                  <option value='date'>Date</option>
                  <option value='category'>Catégorie</option>
                  <option value='title'>Titre</option>
                </Form.Control>
              </Form.Group>
            </Col>
          </Form.Group>
        )}
        {userConnected &&
          userRole === 'admin' &&
          !this.props.minimalView && ( // admin
            <Form.Group as={Row} className='mb-2 mt-3'>
              <Form.Label column xs='3' lg='2' xxl='1' className='mb-2'>
                Trier par :
              </Form.Label>
              <Col xs='9' lg='3' xl='5' xxl='5'>
                <Form.Group controlId='formBasicSelect'>
                  <Form.Control as='select' onChange={(e) => this.props.sortedBy(e)}>
                    <option value='date'>Date</option>
                    <option value='category'>Catégorie</option>
                    <option value='title'>Titre</option>
                  </Form.Control>
                </Form.Group>
              </Col>
              <Col>
                <div key={`inline-radio`} className='d-flex justify-content-between'>
                  <Form.Check inline type='radio' id='inline-radio-1' className='mt-2'>
                    <Form.Check.Input type='radio' name='group1' defaultChecked onChange={this.props.getAll} />
                    <Form.Check.Label>Toutes les sorties</Form.Check.Label>
                  </Form.Check>
                  <Form.Check inline type='radio' id='inline-radio-2' className='mt-2'>
                    <Form.Check.Input type='radio' name='group1' onChange={this.props.getValidate} />
                    <Form.Check.Label>Sorties validées</Form.Check.Label>
                  </Form.Check>
                  <Button variant='outline-dark' onClick={() => this.handleModal()}>
                    Purger les sorties
                  </Button>
                </div>
              </Col>
            </Form.Group>
          )}
        {userConnected && userRole === 'admin' && this.props.minimalView && (
          <Form.Group as={Row} className='mb-2 mt-3'>
            <Form.Label column xs='3' lg='2' xxl='1' className='mb-2'>
              Trier par :
            </Form.Label>
            <Col xs='9' lg='10' xxl='11'>
              <Form.Group controlId='formBasicSelect'>
                <Form.Control as='select' onChange={(e) => this.props.sortedBy(e)}>
                  <option value='date'>Date</option>
                  <option value='category'>Catégorie</option>
                  <option value='title'>Titre</option>
                </Form.Control>
              </Form.Group>
            </Col>
          </Form.Group>
        )}
        <Modal show={this.state.show} onHide={() => this.handleModal()}>
          <Modal.Body>
            <Purgatory handlePurgatory={this.handlePurgatory} token={this.props.token} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default OrderForm;
