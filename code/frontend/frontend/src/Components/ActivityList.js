import React from 'react';
import { Container, Row, Col, Dropdown } from 'react-bootstrap';
import { CSVLink } from 'react-csv';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';
import ActivityCard from './ActivityCard';
import OrderForm from './OrderForm';

class ActivityList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userType: props.userType,
      items: [],
      allTrips: [],
      validateTrips: [],
      showValide: false,
      loaded: false,
      sortedBy: 'date',
      headers: [],
      csvData: [],
      filename: 'liste_activites_validees',
    };
    this.getSortedByValue = this.getSortedByValue.bind(this);
    this.getAll = this.getAll.bind(this);
    this.getValidate = this.getValidate.bind(this);
    this.handleExportPDF = this.handleExportPDF.bind(this);
  }

  componentDidUpdate(prevState) {
    if (this.props.userConnected !== prevState.userConnected) this.fetchActivity();
  }

  componentDidMount() {
    this.fetchActivity();
  }

  fetchActivity() {
    let allTrips = [];
    if (this.props.userConnected && this.props.userRole === 'admin') {
      fetch('http://localhost:8080/trip/getAll')
        .then((res) => res.json())
        .then((json) => {
          if (!json.status) {
            allTrips = this.preprocessingItems(json);
            this.setState({
              items: allTrips,
              allTrips: allTrips,
            });
            const requestOptions = {
              method: 'GET',
              headers: { 'Content-Type': 'application/json', token: this.props.token },
            };
            fetch('http://localhost:8080/trip/getValidate', requestOptions)
              .then((res) => res.json())
              .then((json) => {
                if (!json.status) {
                  let validates = json;
                  let array = [];
                  validates.forEach((validate) => {
                    const trip = allTrips.find((el) => el.idTrip === validate.idTrip);
                    let cloned = JSON.parse(JSON.stringify(trip));
                    array.push(cloned);
                  });
                  let data = [];
                  const headers = [
                    { label: "Nom de l'activité", key: 'title' },
                    { label: 'Date', key: 'date' },
                    { label: 'Catégorie', key: 'category' },
                    { label: 'Adresse', key: 'address' },
                    { label: 'Numéro', key: 'addressNumber' },
                    { label: 'Ville', key: 'city' },
                    { label: 'Nom du responsable', key: 'responsibleName' },
                    { label: 'Prix', key: 'price' },
                  ];
                  array.forEach((a) => {
                    data.push({
                      title: a.title,
                      date: a.tripDate,
                      category: a.categorie.name,
                      address: a.address.address,
                      addressNumber: a.address.addressNumber,
                      city: a.address.location.city,
                      responsibleName: a.responsibleName,
                      price: a.price,
                    });
                  });
                  this.setState({ validateTrips: array, loaded: true, headers: headers, csvData: data });
                }
              });
          } else {
            console.log('erreur chargement des données');
          }
        });
    } else {
      fetch('http://localhost:8080/trip/getAll')
        .then((res) => res.json())
        .then((json) => {
          if (!json.status) {
            allTrips = this.preprocessingItems(json);
            this.setState({
              items: allTrips,
              allTrips: allTrips,
              loaded: true,
            });
          } else {
            console.log('Erreur de chargement des données');
          }
        });
    }
  }

  preprocessingItems(array) {
    let activityList = [];
    let activityArray = JSON.parse(JSON.stringify(array));
    activityArray.forEach((activity) => {
      for (let i = 0; i < activity.tripDate.length; i++) {
        let cloned = JSON.parse(JSON.stringify(activity));
        const date = new Date(activity.tripDate[i].tripDate);
        const day = date.getDate() >= 10 ? date.getDate() : '0' + date.getDate();
        const getMonth = date.getMonth() + 1;
        const month = getMonth > 10 ? getMonth : '0' + getMonth;
        cloned.tripDate = day + '.' + month + '.' + date.getFullYear();
        cloned.allDates = activity.tripDate;
        activityList.push(cloned);
      }
    });
    this.sortList(activityList, 'date');
    return activityList;
  }

  sortList(activityList, sortedBy) {
    if (sortedBy === 'date') {
      activityList = this.sortByDate(activityList);
    } else if (sortedBy === 'category') {
      activityList = this.sortByCategory(activityList);
    } else if (sortedBy === 'title') {
      activityList = this.sortByTitle(activityList);
    }
    return activityList;
  }

  sortByDate(array) {
    array.sort((a, b) => {
      return Date.parse(this.getDate(a.tripDate)) - Date.parse(this.getDate(b.tripDate));
    });
    return array;
  }

  sortByCategory(array) {
    array.sort((a, b) => (a.categorie.name > b.categorie.name ? 1 : -1));
    return array;
  }

  sortByTitle(array) {
    array.sort((a, b) => (a.title > b.title ? 1 : -1));
    return array;
  }

  getDate(d) {
    const split = d.split('.');
    const dd = split[0];
    const mm = split[1];
    const yyyy = split[2];
    return mm + '/' + dd + '/' + yyyy;
  }

  getSortedByValue(e) {
    this.setState({ sortedBy: e.target.value });
    this.sortList(this.state.items, e.target.value);
  }

  getValidate() {
    this.setState({ items: this.state.validateTrips, showValide: true });
  }

  getAll() {
    this.setState({ items: this.state.allTrips, showValide: false });
  }

  handleExportPDF() {
    const doc = new jsPDF('landscape');
    var heads = [];
    var body = [];
    this.state.headers.forEach((e) => {
      heads.push(e.label);
    });
    this.state.csvData.forEach((e) => {
      var tmp = [];
      tmp.push(e.title, e.date, e.category, e.address, e.addressNumber, e.city, e.responsibleName, e.price);
      body.push(tmp);
    });
    doc.text('Liste des activités validées', doc.internal.pageSize.getWidth() / 2, 10, { align: 'center' });
    doc.autoTable({
      head: [heads],
      body: body,
    });
    doc.save(this.state.filename + '.pdf');
  }

  render() {
    const loaded = this.state.loaded;
    const userConnected = this.props.userConnected;
    const userRole = this.props.userRole;
    if (!loaded) return <div>Loading please wait</div>;
    return (
      <Container className='mb-3 p-4'>
        <OrderForm
          userConnected={userConnected}
          userRole={userRole}
          token={this.props.token}
          sortedBy={this.getSortedByValue}
          minimalView={false}
          getAll={this.getAll}
          getValidate={this.getValidate}
        />
        {userConnected && userRole === 'admin' && this.state.showValide && (
          <Row>
            <Col md='10'></Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: '100%' }} variant='success' id='btn-export'>
                  Exporter
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    as={CSVLink}
                    filename={this.state.filename + '.csv'}
                    data={this.state.csvData}
                    headers={this.state.headers}
                    separator={';'}
                  >
                    CSV
                  </Dropdown.Item>
                  <Dropdown.Item onClick={this.handleExportPDF}>PDF</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
        )}
        <Row xs={1} md={2} lg={3} xl={4} className='p-0 justify-content-center'>
          {this.state.items.map((activity) => (
            <ActivityCard
              key={activity.idTrip + '/' + activity.tripDate}
              idTrip={activity.idTrip}
              image={activity.image}
              title={activity.title}
              descriptionXml={activity.descriptionXml}
              date={activity.tripDate}
              allDates={activity.allDates}
              category={activity.categorie.name}
              address={activity.address.address}
              addressNumber={activity.address.addressNumber}
              city={activity.address.location.city}
              country={activity.address.location.country}
              postcode={activity.address.location.postcode}
              responsibleName={activity.responsibleName}
              responsibleSurname={activity.responsibleSurname}
              min={activity.minInscription}
              max={activity.maxInscription}
              identityDocuments={activity.identityDocument}
              price={activity.price}
              language={activity.language}
              video={activity.video}
              version={activity.version}
            />
          ))}
        </Row>
      </Container>
    );
  }
}

export default ActivityList;
