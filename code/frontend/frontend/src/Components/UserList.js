import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Card, Col } from 'react-bootstrap';

class UserListComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      loaded: false,
    };
  }

  componentDidUpdate(prevState) {
    if (this.props.userConnected !== prevState.userConnected) this.props.navigate('/');
  }

  componentDidMount() {
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json', token: this.props.token },
    };
    fetch('http://localhost:8080/account/getUsers', requestOptions)
      .then((res) => res.json())
      .then((json) => {
        let res = [];
        for (let i = 0; i < json.length; i++) {
          res.push(json[i].person);
        }
        this.setState({ userList: res, loaded: true });
      });
  }

  handleClick(index) {
    this.props.navigate('/myActivities', {
      replace: true,
      state: {
        firstname: this.state.userList[index].firstName,
        lastname: this.state.userList[index].lastName,
        userEmail: this.state.userList[index].email,
        fromUserList: true,
      },
    });
  }

  render() {
    if (!this.state.loaded) return <div>Chargement des utilisateurs</div>;
    return (
      <Container className='mt-3'>
        <h2 className='mb-3'>Liste des utilisateurs</h2>
        {this.state.userList.map((user, index) => (
          <Card className='rounded-0' key={index}>
            <Card.Body onClick={(e) => this.handleClick(index)}>
              <div className='d-flex justify-content-between'>
                <Col xs={6}>
                  <div>
                    {user.lastName} {user.firstName}
                  </div>
                </Col>
                <Col>
                  <div className='d-flex justify-content-end'>{user.email}</div>
                </Col>
              </div>
            </Card.Body>
          </Card>
        ))}
      </Container>
    );
  }
}

function UserList(props) {
  let navigate = useNavigate();
  return <UserListComponent {...props} navigate={navigate} />;
}

export default UserList;
