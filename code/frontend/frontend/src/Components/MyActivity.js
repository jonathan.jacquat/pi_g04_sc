import React from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Container, Row, Col, CardGroup, Button, Dropdown } from 'react-bootstrap';
import ActivityCard from './ActivityCard';
import OrderForm from './OrderForm';
import { CSVLink } from 'react-csv';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

class MyActivityComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userType: props.userType,
      myActivities: [],
      loaded: false,
      sortedBy: 'date',
      headers: [],
      csvData: [],
      filename: this.props.location ? 'activites_' + this.props.location.lastname + '_' + this.props.location.firstname : '',
    };
    this.getSortedByValue = this.getSortedByValue.bind(this);
    this.handleExportPDF = this.handleExportPDF.bind(this);
  }

  goBack = () => {
    this.props.navigate('/userList');
  };

  componentDidMount() {
    this.fetchActivities();
  }

  componentDidUpdate(prevState) {
    if (this.props.userConnected !== prevState.userConnected) this.props.navigate('/');
  }

  fetchActivities() {
    fetch('http://localhost:8080/trip/getAll')
      .then((res) => res.json())
      .then((json) => {
        const headers = [
          { label: "Nom de l'activité", key: 'title' },
          { label: 'Date', key: 'date' },
          { label: 'Catégorie', key: 'category' },
          { label: 'Adresse', key: 'address' },
          { label: 'Numéro', key: 'addressNumber' },
          { label: 'Ville', key: 'city' },
          { label: 'Nom du responsable', key: 'responsibleName' },
          { label: 'Prix', key: 'price' },
        ];
        let activities = json;
        const userEmail = this.props.location === null ? this.props.userEmail : this.props.location.userEmail;
        const requestOptions = {
          method: 'GET',
          headers: { 'Content-Type': 'application/json', token: this.props.token },
        };
        fetch('http://localhost:8080/trip/getRegistration/' + userEmail, requestOptions)
          .then((res) => res.json())
          .then((json) => {
            let mines = [];
            let array = JSON.parse(JSON.stringify(json));
            const data = [];
            mines = this.preprocessingItems(activities, array);
            mines.forEach((a) => {
              data.push({
                title: a.title,
                date: a.tripDate,
                category: a.categorie.name,
                address: a.address.address,
                addressNumber: a.address.addressNumber,
                city: a.address.location.city,
                responsibleName: a.responsibleName,
                price: a.price,
              });
            });
            this.setState({
              headers: headers,
              csvData: data,
              myActivities: mines,
              loaded: true,
            });
          });
      });
  }

  preprocessingItems(activities, mines) {
    let activityList = [];
    mines.forEach((mine) => {
      activities.forEach((activity) => {
        for (let i = 0; i < activity.tripDate.length; i++) {
          if (mine.idTrip === activity.idTrip) {
            let cloned = JSON.parse(JSON.stringify(activity));
            const date = new Date(activity.tripDate[i].tripDate);
            const day = date.getDate() >= 10 ? date.getDate() : '0' + date.getDate();
            const getMonth = date.getMonth() + 1;
            const month = getMonth > 10 ? getMonth : '0' + getMonth;
            cloned.tripDate = day + '.' + month + '.' + date.getFullYear();
            cloned.address.address = activity.address.address + ' ' + activity.address.addressNumber;
            cloned.address.location.city =
              activity.address.location.postcode + ' ' + activity.address.location.city + ', ' + activity.address.location.country;
            cloned.responsibleName = activity.responsibleName + ' ' + activity.responsibleSurname;
            activityList.push(cloned);
          }
        }
      });
    });

    if (this.state.sortedBy === 'date') {
      activityList.sort((a, b) => {
        return Date.parse(this.getDate(a.tripDate)) - Date.parse(this.getDate(b.tripDate));
      });
    } else if (this.state.sortedBy === 'category') {
      activityList.sort((a, b) => (a.categorie.name > b.categorie.name ? 1 : -1));
    } else if (this.state.sortedBy === 'title') {
      activityList.sort((a, b) => (a.title > b.title ? 1 : -1));
    }
    return activityList;
  }

  getDate(d) {
    const split = d.split('.');
    const dd = split[0];
    const mm = split[1];
    const yyyy = split[2];
    return mm + '/' + dd + '/' + yyyy;
  }

  getSortedByValue(e) {
    this.setState({ sortedBy: e.target.value });
  }

  handleExportPDF() {
    const doc = new jsPDF('landscape');
    var heads = [];
    var body = [];
    this.state.headers.forEach((e) => {
      heads.push(e.label);
    });
    this.state.csvData.forEach((e) => {
      var tmp = [];
      tmp.push(e.title, e.date, e.category, e.address, e.addressNumber, e.city, e.responsibleName, e.price);
      body.push(tmp);
    });
    doc.text("Liste des activités de l'utilisateur : " + this.props.location.userEmail, doc.internal.pageSize.getWidth() / 2, 10, {
      align: 'center',
    });
    doc.autoTable({
      head: [heads],
      body: body,
    });
    doc.save(this.state.filename + '.pdf');
  }

  render() {
    const loaded = this.state.loaded;
    const userConnected = this.props.userConnected;
    const userRole = this.props.userRole;
    if (!loaded) return <div>Loading please wait</div>;
    return (
      <Container className='mb-3'>
        {this.props.location !== null && this.props.location.fromUserList && (
          <Row className='mt-3'>
            <Col xs='1'>
              <Button variant='outline-secondary' onClick={this.goBack}>
                Retour
              </Button>
            </Col>
            <Col xs='2'>
              <Dropdown>
                <Dropdown.Toggle style={{ width: '100%' }} variant='success' id='btn-export'>
                  Exporter
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    as={CSVLink}
                    filename={this.state.filename + '.csv'}
                    data={this.state.csvData}
                    headers={this.state.headers}
                    separator={';'}
                  >
                    CSV
                  </Dropdown.Item>
                  <Dropdown.Item onClick={this.handleExportPDF}>PDF</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
        )}
        <OrderForm userConnected={userConnected} userRole={userRole} sortedBy={this.getSortedByValue} minimalView={true} />
        <CardGroup>
          <Row xs={1} md={2} lg={3} xl={4} className='g-4'>
            {this.state.myActivities.map((activity) => (
              <Col key={activity.idTrip}>
                <ActivityCard
                  idTrip={activity.idTrip}
                  image={activity.image}
                  title={activity.title}
                  descriptionXml={activity.descriptionXml}
                  date={activity.tripDate}
                  category={activity.categorie.name}
                  address={activity.address.address}
                  localisation={activity.address.location.city}
                  responsibleName={activity.responsibleName}
                  min={activity.minInscription}
                  max={activity.maxInscription}
                  identityDocuments={activity.identityDocument}
                  price={activity.price}
                />
              </Col>
            ))}
          </Row>
        </CardGroup>
      </Container>
    );
  }
}

function MyActivity(props) {
  const { state } = useLocation();
  let navigate = useNavigate();
  return <MyActivityComponent {...props} navigate={navigate} location={state} />;
}

export default MyActivity;
