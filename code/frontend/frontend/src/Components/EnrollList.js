import React from 'react';
import { ListGroup, Row, Col, Button, Dropdown } from 'react-bootstrap';
import { CSVLink } from 'react-csv';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

class EnrollList extends React.Component {
  constructor(props) {
    super(props);
    let date = this.props.date.split('.');
    this.state = {
      headers: [],
      csvData: [],
      filename: 'inscription_' + this.props.title.replace(/ /g, '_') + '_' + date[0] + '-' + date[1] + '-' + date[2],
    };
    this.handleExportPDF = this.handleExportPDF.bind(this);
  }

  componentDidMount() {
    const headers = [
      { label: "Nom de l'activité", key: 'title' },
      { label: 'Catégorie', key: 'category' },
      { label: 'Langue', key: 'language' },
      { label: 'Email', key: 'email' },
      { label: 'Prix total', key: 'totalPrice' },
    ];
    const data = [];
    this.props.enrollList.forEach((u) => {
      data.push({
        title: this.props.title,
        category: this.props.category,
        language: this.props.language,
        email: u,
        totalPrice: this.props.price * this.props.enrollList.length,
      });
    });
    this.setState({ headers: headers, csvData: data });
  }

  handleExportPDF() {
    const doc = new jsPDF('landscape');
    var heads = [];
    var body = [];
    this.state.headers.forEach((e) => {
      heads.push(e.label);
    });
    this.state.csvData.forEach((e) => {
      var tmp = [];
      tmp.push(e.title, e.category, e.language, e.email, e.totalPrice);
      body.push(tmp);
    });
    doc.text("Liste des inscription pour l'activité : " + this.props.title, doc.internal.pageSize.getWidth() / 2, 10, { align: 'center' });
    doc.autoTable({
      head: [heads],
      body: body,
    });
    doc.save(this.state.filename + '.pdf');
  }

  render() {
    this.props.enrollList.sort((a, b) => (a.lastname > b.lastname ? 1 : -1));
    return (
      <div>
        <h4>Liste des inscriptions</h4>
        <ListGroup className='mt-3'>
          {this.props.enrollList.map((u, index) => (
            <ListGroup.Item key={index}>{u}</ListGroup.Item>
          ))}
        </ListGroup>
        <Row className=' mt-2'>
          <Col as='div' className='d-flex justify-content-end fs-5'>
            <div className='me-1'>
              <strong className='float-right'>Prix total : </strong>
            </div>
            <div>{this.props.price * this.props.enrollList.length}.-</div>
          </Col>
        </Row>
        <Row className='ms-1 me-1 mt-1'>
          <Col>
            <Dropdown>
              <Dropdown.Toggle style={{ width: '100%' }} variant='success' id='btn-export'>
                Exporter
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  as={CSVLink}
                  filename={this.state.filename + '.csv'}
                  data={this.state.csvData}
                  headers={this.state.headers}
                  separator={';'}
                >
                  CSV
                </Dropdown.Item>
                <Dropdown.Item onClick={this.handleExportPDF}>PDF</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col>
            <Button style={{ width: '100%' }} variant='info' onClick={() => this.props.handleModalEnrollList()}>
              OK
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default EnrollList;
