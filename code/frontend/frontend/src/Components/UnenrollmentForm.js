import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';

class UnenrollmentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      unenrolledPerson: [],
    };
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSelect(person) {
    this.setState({ unenrolledPerson: [...this.state.unenrolledPerson, person] });
  }

  handleSubmit() {
    this.props.handleUnenroll(this.state.unenrolledPerson);
  }

  render() {
    return (
      <Form style={{ userSelect: 'none' }}>
        {this.props.enrolledList.map((person, index) => (
          <Form.Group className='mb-3' controlId={index} key={index}>
            <Form.Check type='checkbox' label={person} onClick={() => this.handleSelect(person)} />
          </Form.Group>
        ))}
        <Button variant='success' onClick={this.handleSubmit}>
          Désinscrire
        </Button>
      </Form>
    );
  }
}

export default UnenrollmentForm;
