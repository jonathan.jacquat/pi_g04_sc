import React from 'react';
import { Container, Row, Button, Form, Table } from 'react-bootstrap';

class PurgatoryComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userType: props.userType,
      toPurge: [],
      loaded: false,
      sortedBy: 'date',
      checked: false,
      selectedActivities: [],
    };
  }

  componentDidMount() {
    this.fetchActivityToPurge();
  }

  fetchActivityToPurge() {
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json', token: this.props.token },
    };
    fetch('http://localhost:8080/trip/purge', requestOptions)
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          toPurge: json,
          loaded: true,
        });
      });
  }

  purgeActivities = () => {
    let activitiesToPurge = [...this.state.selectedActivities];
    let readyToPurge = [];

    activitiesToPurge.forEach((activity) => {
      readyToPurge.push({
        idTrip: activity.tripID,
        tripName: activity.tripName,
        tripDate: activity.tripDate,
      });
    });

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', token: this.props.token },
      body: JSON.stringify(readyToPurge),
    };

    fetch('http://localhost:8080/trip/purge', requestOptions)
      .then((res) => res)
      .then((res) => {
        if (res.status === 200) {
          this.props.handlePurgatory();
        } else {
          console.log(res);
        }
      });
  };

  selectActivityToPurge = (tripID, tripName, tripDate) => {
    this.setState({
      selectedActivities: [{ tripID: tripID, tripName: tripName, tripDate: tripDate }, ...this.state.selectedActivities],
    });

    let array = [...this.state.selectedActivities];

    array.forEach((activity) => {
      if (activity.tripID === tripID && activity.tripDate === tripDate) {
        const index = this.state.selectedActivities.findIndex((activity) => activity.tripID === tripID && activity.tripDate === tripDate);

        if (index !== -1) {
          array.splice(index, 1);
          this.setState({ selectedActivities: array });
        }
      }
    });
  };

  render() {
    const loaded = this.state.loaded;

    if (!loaded) return <div>Loading please wait</div>;
    let activityList = JSON.parse(JSON.stringify(this.state.toPurge));
    return (
      <Container className='mb-3 p-4'>
        <Row>
          <Table responsive>
            <thead>
              <tr>
                <th key={1}>Titre</th>
                <th key={2}>Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {activityList.map((activity, index) => (
                <tr key={index}>
                  <td>{activity.tripName}</td>
                  <td>{activity.tripDate}</td>
                  <td>
                    <Form>
                      <Form.Check
                        type='switch'
                        id={`switch-${index}`}
                        label=''
                        onChange={() => this.selectActivityToPurge(activity.idTrip, activity.tripName, activity.tripDate)}
                      />
                    </Form>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Row>

        <Row className='mb-3 p-4'>
          <Button variant='outline-secondary' onClick={() => this.purgeActivities()}>
            Supprimer
          </Button>
        </Row>
      </Container>
    );
  }
}

export default PurgatoryComponent;
