import React from 'react';
import { Navbar, Nav, Container, Modal, Button } from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/LinkContainer';
import Login from './Login';

class Navigation extends React.Component {
	constructor(props) {
		super(props);
		this.handleLogin = this.handleLogin.bind(this);
	}

	handleLogin(email, password) {
		this.props.handleLogin(email, password);
		if (!this.props.showLoginError) this.setState({ show: false });
	}

	render() {
		const userConnected = this.props.userConnected;
		const userRole = this.props.userRole;
		return (
			<div>
				<Navbar bg='light' expand='lg'>
					<Container>
						<LinkContainer to='/'>
							<Navbar.Brand href='#home' className='fs-1'>
								Sorties Culturelles
							</Navbar.Brand>
						</LinkContainer>
						<Navbar.Toggle aria-controls='responsive-navbar-nav' />
						<Navbar.Collapse id='responsive-navbar-nav'>
							{!userConnected && (
								<Nav className='me-auto justify-content-end fs-5' style={{ width: '100%' }}>
									<Button
										onClick={() => {
											this.props.handleModalLogin();
										}}
									>
										Se connecter
									</Button>
								</Nav>
							)}
							{userConnected &&
								userRole === 'client' && ( // Logged
									<Nav className='me-auto justify-content-end fs-5' style={{ width: '100%' }}>
										<LinkContainer to='/myActivities'>
											<Nav.Link href='/mesActivites' eventKey='myActivities'>
												Mes sorties culturelles
											</Nav.Link>
										</LinkContainer>
										<Nav.Link onClick={() => this.props.handleLogout()}>Se déconnecter</Nav.Link>
									</Nav>
								)}
							{userConnected &&
								userRole === 'admin' && ( // admin
									<Nav className='la-auto justify-content-end fs-5' style={{ width: '100%' }}>
										<LinkContainer to='/addActivity'>
											<Nav.Link>Créer une nouvelle sortie culturelle</Nav.Link>
										</LinkContainer>
										<LinkContainer to='/userList'>
											<Nav.Link>Utilisateurs</Nav.Link>
										</LinkContainer>
										<Nav.Link onClick={() => this.props.handleLogout()}>Se déconnecter</Nav.Link>
									</Nav>
								)}
						</Navbar.Collapse>
					</Container>
				</Navbar>
				<Modal show={this.props.showModalLogin} onHide={() => this.props.handleModalLogin()}>
					<Modal.Body>
						<Login handleLogin={this.handleLogin} showLoginError={this.props.showLoginError} />
					</Modal.Body>
				</Modal>
			</div>
		);
	}
}

export default Navigation;
