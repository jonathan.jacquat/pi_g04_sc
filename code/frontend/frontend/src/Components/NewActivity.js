import React from 'react';
import { Container, Form, Row, Col, Button, Alert } from 'react-bootstrap';
import { useNavigate, useLocation } from 'react-router';
import { Buffer } from 'buffer';
import NewActivityForm from './NewActivityForm';

class NewActivityComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			idTrip: 0,
			title: '',
			category: '',
			min: '',
			max: '',
			price: '',
			responsibleName: '',
			responsibleSurname: '',
			address: '',
			addressNumber: '',
			city: '',
			postcode: '',
			country: '',
			image: null,
			identityDocument: '',
			identityDocumentNumber: 3,
			date: '',
			selectedDate: this.props.location ? this.props.location.selectedDate : '',
			dateNumber: 3,
			language: '',
			desc: '',
			schedule: '',
			scheduleNumber: 3,
			imageUrl: '',
			video: '',
			version: this.props.version,
			versionError: false,
			versionErrorMessage: '',
			newVersion: {},
		};
		this.handleImageChange = this.handleImageChange.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleModify = this.handleModify.bind(this);
	}

	componentDidMount() {
		const loc = this.props.location;
		if (loc !== null && (loc.duplicate || loc.modify)) {
			let idDocs = '';
			for (let i = 0; i < loc.identityDocument.length; i++) {
				if (i < loc.identityDocument.length - 1) {
					idDocs += loc.identityDocument[i].name + '\n';
				} else {
					idDocs += loc.identityDocument[i].name;
				}
			}
			let schedules = '';
			for (let i = 0; i < loc.schedule.length; i++) {
				if (i < loc.schedule.length - 1) {
					schedules += loc.schedule[i].hour + ', ' + loc.schedule[i].text + '\n';
				} else {
					schedules += loc.schedule[i].hour + ', ' + loc.schedule[i].text;
				}
			}
			let dates = '';
			for (let i = 0; i < loc.date.length; i++) {
				const dateSplit = loc.date[i].tripDate.split('-');
				if (i < loc.date.length - 1) {
					dates += dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0] + '\n';
				} else {
					dates += dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];
				}
			}
			this.setState({
				idTrip: loc.idTrip,
				title: loc.title,
				category: loc.category,
				min: loc.min,
				max: loc.max,
				price: loc.price,
				responsibleName: loc.responsibleName,
				responsibleSurname: loc.responsibleSurname,
				address: loc.address,
				addressNumber: loc.addressNumber,
				city: loc.city,
				postcode: loc.postcode,
				country: loc.country,
				image: loc.image,
				identityDocument: idDocs,
				identityDocumentNumber: loc.identityDocument.length < 3 ? 3 : loc.identityDocument.length,
				date: dates,
				dateNumber: loc.date.length < 3 ? 3 : loc.date.length,
				language: loc.language,
				desc: loc.desc,
				schedule: schedules,
				scheduleNumber: loc.schedule.length < 3 ? 3 : loc.schedule.length,
				imageUrl: `data:image/jpeg;base64,${loc.image}`,
				video: loc.video,
				version: loc.version,
			});
		}
	}

	componentDidUpdate(prevState) {
		if (this.props.userConnected !== prevState.userConnected) this.props.navigate('/');
	}

	handleImageChange(event) {
		this.imageToByteArray(event.target.files[0]);
	}

	handleChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	}

	handleSubmit() {
		const xml = this.xmlPreparation();
		const idList = this.state.identityDocument.split('\n');
		const identityDocs = [];
		for (let i = 0; i < idList.length; i++) {
			identityDocs.push(JSON.parse(JSON.stringify({ name: idList[i] })));
		}
		const dateList = this.state.date.split('\n');
		const dates = [];
		for (let i = 0; i < dateList.length; i++) {
			const date = dateList[i].split('-');
			dates.push(JSON.parse(JSON.stringify({ tripDate: date[2] + '-' + date[1] + '-' + date[0] })));
		}
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', token: this.props.token },
			body: JSON.stringify({
				categorie: { name: this.state.category },
				address: {
					address: this.state.address,
					addressNumber: this.state.addressNumber,
					location: {
						postcode: this.state.postcode,
						country: this.state.country,
						city: this.state.city,
					},
				},
				identityDocument: identityDocs,
				tripDate: dates,
				title: this.state.title,
				maxInscription: this.state.max,
				minInscription: this.state.min,
				responsibleName: this.state.responsibleName,
				responsibleSurname: this.state.responsibleSurname,
				price: this.state.price,
				descriptionXml: xml,
				image: this.state.image,
				video: this.state.video,
			}),
		};
		console.log('here');
		fetch('http://localhost:8080/trip/create', requestOptions)
			.then((res) => res.json())
			.then((json) => {
				if (!json.status) {
					this.props.navigate('/', {
						replace: false,
						state: {
							userType: this.props.userType,
							items: [],
							loaded: false,
							sortedBy: 'date',
						},
					});
				} else {
					console.log(json.text());
				}
			});
	}

	handleModify() {
		const xml = this.xmlPreparation();
		const idList = this.state.identityDocument.split('\n');
		const identityDocs = [];
		for (let i = 0; i < idList.length; i++) {
			identityDocs.push(JSON.parse(JSON.stringify({ name: idList[i] })));
		}
		const dateList = this.state.date.split('\n');
		const dates = [];
		for (let i = 0; i < dateList.length; i++) {
			const date = dateList[i].split('-');
			dates.push(JSON.parse(JSON.stringify({ tripDate: date[2] + '-' + date[1] + '-' + date[0] })));
		}
		const requestOptions = {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json', token: this.props.token },
			body: JSON.stringify({
				idTrip: this.state.idTrip,
				categorie: { name: this.state.category },
				address: {
					address: this.state.address,
					addressNumber: this.state.addressNumber,
					location: {
						postcode: this.state.postcode,
						country: this.state.country,
						city: this.state.city,
					},
				},
				identityDocument: identityDocs,
				tripDate: dates,
				title: this.state.title,
				maxInscription: this.state.max,
				minInscription: this.state.min,
				responsibleName: this.state.responsibleName,
				responsibleSurname: this.state.responsibleSurname,
				price: this.state.price,
				descriptionXml: xml,
				image: this.state.image,
				video: this.state.video,
				version: this.state.version,
			}),
		};
		console.log('modify');
		fetch('http://localhost:8080/trip/update', requestOptions).then((res) => {
			if (res.status !== 422) {
				res.json().then((json) => {
					let desc = '';
					let language = '';
					let schedules = [];
					const descXml = Buffer.from(json.descriptionXml, 'base64').toString();
					if (descXml !== '') {
						const parser = new DOMParser();
						const xml = parser.parseFromString(descXml, 'text/xml');
						desc = xml.getElementsByTagName('resume')[0].childNodes[0].nodeValue;
						language = xml.getElementsByTagName('language')[0].childNodes[0].nodeValue;
						const scheduleList = xml.getElementsByTagName('schedule')[0].children;
						for (let i = 0; i < scheduleList.length; i++) {
							let hour = scheduleList[i].children[0].textContent.split(':');
							let hourSplit = hour[0] + ':' + hour[1];
							let text = scheduleList[i].children[1].textContent;
							schedules.push(JSON.parse(JSON.stringify({ hour: hourSplit, text: text })));
						}
					}
					const sepDate = this.state.selectedDate.split('.');
					const splitDate = sepDate[2] + '-' + sepDate[1] + '-' + sepDate[0];
					const arraySplit = json.tripDate[0].tripDate.split('-');
					const newDate = arraySplit[2] + '.' + arraySplit[1] + '.' + arraySplit[0];
					const select = json.tripDate.includes(splitDate) ? this.state.selectedDate : newDate;
					this.props.navigate('/activityDetails', {
						replace: true,
						state: {
							idTrip: json.idTrip,
							image: json.image,
							title: json.title,
							date: select,
							allDates: json.tripDate,
							category: json.categorie.name,
							address: json.address.address,
							addressNumber: json.address.addressNumber,
							city: json.address.location.city,
							country: json.address.location.country,
							postcode: json.address.location.postcode,
							responsibleName: json.responsibleName,
							responsibleSurname: json.responsibleSurname,
							min: json.minInscription,
							max: json.maxInscription,
							identityDocuments: json.identityDocument,
							price: json.price,
							desc: desc,
							language: language,
							schedules: schedules,
							video: json.video,
							version: json.version,
						},
					});
				});
			} else if (res.status === 422) {
				res.json().then((json) => {
					const activity = this.prepareNewVersion(JSON.parse(JSON.stringify(json)));
					activity.image = `data:image/jpeg;base64,${activity.image}`;
					this.setState({ versionError: true, versionErrorMessage: 'erreur de version', newVersion: activity, version: activity.version });
				});
			} else {
				console.log(res.text());
			}
		});
	}

	prepareNewVersion(act) {
		let desc = '';
		let language = '';
		let schedules = '';
		let scheduleLength = 0;
		const descXml = Buffer.from(act.descriptionXml, 'base64').toString();
		if (descXml !== '') {
			const parser = new DOMParser();
			const xml = parser.parseFromString(descXml, 'text/xml');
			desc = xml.getElementsByTagName('resume')[0].childNodes[0].nodeValue;
			language = xml.getElementsByTagName('language')[0].childNodes[0].nodeValue;
			const scheduleList = xml.getElementsByTagName('schedule')[0].children;
			scheduleLength = scheduleList.length;
			for (let i = 0; i < scheduleList.length; i++) {
				let hour = scheduleList[i].children[0].textContent.split(':');
				let hourSplit = hour[0] + ':' + hour[1];
				let text = scheduleList[i].children[1].textContent;
				if (i < scheduleList.length - 1) {
					schedules += hourSplit + ', ' + text + '\n';
				} else {
					schedules += hourSplit + ', ' + text;
				}
			}
		}
		let docs = '';
		let docsLength = act.identityDocument.length;
		for (let i = 0; i < act.identityDocument.length; i++) {
			if (i < act.identityDocument.length - 1) {
				docs += act.identityDocument[i].name + '\n';
			} else {
				docs += act.identityDocument[i].name;
			}
		}
		let dates = '';
		let dateLength = act.tripDate.length;
		for (let i = 0; i < act.tripDate.length; i++) {
			const date = act.tripDate[i].tripDate.split('-');
			if (i < act.tripDate.length - 1) {
				dates += date[2] + '-' + date[1] + '-' + date[0] + '\n';
			} else {
				dates += date[2] + '-' + date[1] + '-' + date[0];
			}
		}
		const description = JSON.parse(
			JSON.stringify({
				desc: desc,
				language: language,
				schedule: schedules,
				scheduleNumber: scheduleLength < 3 ? 3 : scheduleLength,
				identityDocument: docs,
				identityDocumentNumber: docsLength < 3 ? 3 : docsLength,
				dateNumber: dateLength < 3 ? 3 : dateLength,
			})
		);
		act.image = act.image;
		act.descriptionXml = description;
		act.tripDate = dates;
		return act;
	}

	imageToByteArray(image) {
		if (image) {
			const reader = new FileReader();
			const imageByteArray = [];
			reader.readAsArrayBuffer(image);
			reader.onloadend = (e) => {
				if (e.target.readyState === FileReader.DONE) {
					const arrayBuffer = e.target.result,
						array = new Uint8Array(arrayBuffer);
					for (const a of array) {
						imageByteArray.push(a);
					}
					this.setState({ image: imageByteArray });
				}
			};
			const readerUrl = new FileReader();
			readerUrl.readAsDataURL(image);
			readerUrl.onloadend = (e) => {
				if (e.target.readyState === FileReader.DONE) {
					this.setState({ imageUrl: readerUrl.result });
				}
			};
		} else {
			this.setState({ imageUrl: '' });
		}
	}

	xmlEncoding(value) {
		return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
	}

	xmlPreparation() {
		let resXML = '<?xml version="1.0" encoding="UTF-8"?><trip xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="description.xsd">';
		const language = '<language>' + this.xmlEncoding(this.state.language) + '</language>';
		const resume = '<resume>' + this.xmlEncoding(this.state.desc) + '</resume>';
		const scheduleList = this.state.schedule.split('\n');
		let schedules = '<schedule>';
		for (let i = 0; i < scheduleList.length; i++) {
			const splitted = scheduleList[i].split(', ', 2);
			const schedule = JSON.parse('{"hour": "' + this.xmlEncoding(splitted[0]) + '", "text": "' + this.xmlEncoding(splitted[1]) + '"}');
			schedules = schedules + '<schedule_item><hour>' + schedule.hour + ':00</hour><text>' + schedule.text + '</text></schedule_item>';
		}
		schedules = schedules + '</schedule>';
		resXML = resXML + language + resume + schedules + '</trip>';
		return Buffer.from(resXML).toString('base64');
	}

	goBack = () => {
		this.props.navigate(-1);
	};

	render() {
		return (
			<Container className='mb-3'>
				<Form>
					<Row className='mt-2 mb-3'>
						<Col xs='2'>
							<Button variant='outline-secondary' onClick={this.goBack}>
								Retour
							</Button>
						</Col>
						<Col>
							<Row>
								{!this.props.location ||
									(!this.props.location.modify && (
										<div className='fs-2'>
											<strong>Créer une nouvelle sortie culturelle</strong>
										</div>
									))}
								{this.props.location && this.props.location.modify && (
									<div className='fs-2'>
										<strong>Modifier une sortie culturelle</strong>
									</div>
								)}
							</Row>
						</Col>
					</Row>
					{this.state.versionError && (
						<Row>
							<Alert variant='danger'>Vos modifications rentrent en conflits avec les dernières modifications sauvegardées</Alert>
							<Col>
								<NewActivityForm
									title={this.state.title}
									category={this.state.category}
									min={this.state.min}
									max={this.state.max}
									price={this.state.price}
									responsibleSurname={this.state.responsibleSurname}
									responsibleName={this.state.responsibleName}
									address={this.state.address}
									addressNumber={this.state.addressNumber}
									city={this.state.city}
									postcode={this.state.postcode}
									country={this.state.country}
									imageUrl={this.state.imageUrl}
									handleImageChange={this.handleImageChange}
									video={this.state.video}
									identityDocumentNumber={this.state.identityDocumentNumber}
									identityDocument={this.state.identityDocument}
									date={this.state.date}
									dateNumber={this.state.dateNumber}
									language={this.state.language}
									desc={this.state.desc}
									scheduleNumber={this.state.scheduleNumber}
									schedule={this.state.schedule}
									handleChange={this.handleChange}
									newVersion={false}
								/>
							</Col>
							<Col>
								<NewActivityForm
									title={this.state.newVersion.title}
									category={this.state.newVersion.categorie.name}
									min={this.state.newVersion.minInscription}
									max={this.state.newVersion.maxInscription}
									price={this.state.newVersion.price}
									responsibleSurname={this.state.newVersion.responsibleSurname}
									responsibleName={this.state.newVersion.responsibleName}
									address={this.state.newVersion.address.address}
									addressNumber={this.state.newVersion.address.addressNumber}
									city={this.state.newVersion.address.location.city}
									postcode={this.state.newVersion.address.location.postcode}
									country={this.state.newVersion.address.location.country}
									image={this.state.newVersion.image}
									video={this.state.newVersion.video}
									identityDocumentNumber={this.state.newVersion.descriptionXml.identityDocumentNumber}
									identityDocument={this.state.newVersion.descriptionXml.identityDocument}
									date={this.state.newVersion.tripDate}
									dateNumber={this.state.newVersion.descriptionXml.dateNumber}
									language={this.state.newVersion.descriptionXml.language}
									desc={this.state.newVersion.descriptionXml.desc}
									scheduleNumber={this.state.newVersion.descriptionXml.scheduleNumber}
									schedule={this.state.newVersion.descriptionXml.schedule}
									newVersion={true}
								/>
							</Col>
						</Row>
					)}
					{!this.state.versionError && (
						<NewActivityForm
							title={this.state.title}
							category={this.state.category}
							min={this.state.min}
							max={this.state.max}
							price={this.state.price}
							responsibleSurname={this.state.responsibleSurname}
							responsibleName={this.state.responsibleName}
							address={this.state.address}
							addressNumber={this.state.addressNumber}
							city={this.state.city}
							postcode={this.state.postcode}
							country={this.state.country}
							imageUrl={this.state.imageUrl}
							handleImageChange={this.handleImageChange}
							video={this.state.video}
							identityDocumentNumber={this.state.identityDocumentNumber}
							identityDocument={this.state.identityDocument}
							date={this.state.date}
							dateNumber={this.state.dateNumber}
							language={this.state.language}
							desc={this.state.desc}
							scheduleNumber={this.state.scheduleNumber}
							schedule={this.state.schedule}
							handleChange={this.handleChange}
							newVersion={false}
						/>
					)}
					<Row>
						{(!this.props.location || (this.props.location && !this.props.location.modify)) && (
							<Button variant='dark' onClick={this.handleSubmit}>
								Ajouter la sortie
							</Button>
						)}
						{this.props.location && this.props.location.modify && (
							<Button variant='dark' onClick={this.handleModify}>
								Modifier la sortie
							</Button>
						)}
					</Row>
				</Form>
			</Container>
		);
	}
}

function NewActivity(props) {
	const { state } = useLocation();
	let navigate = useNavigate();
	return <NewActivityComponent {...props} location={state} navigate={navigate} />;
}

export default NewActivity;
