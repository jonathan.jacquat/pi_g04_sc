import React, { Component } from 'react';
import { Form, Button, Alert } from 'react-bootstrap';
export default class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	}

	handleSubmit() {
		this.props.handleLogin(this.state.email, this.state.password);
	}

	render() {
		return (
			<Form>
				{this.props.showLoginError && <Alert variant='danger'>Nom d'utilisateur ou mot de passe éronné</Alert>}
				<Form.Label size='lg' className='mb-2'>
					Connexion
				</Form.Label>
				<Form.Group className='mb-2' controlId='loginEmail'>
					<Form.Label>Email</Form.Label>
					<Form.Control type='email' placeholder='Entrer votre adresse mail' name='email' onChange={this.handleChange} />
				</Form.Group>
				<Form.Group className='mb-2' controlId='loginPassword'>
					<Form.Label>Mot de passe</Form.Label>
					<Form.Control type='password' placeholder='Entrer votre mot de passe' name='password' onChange={this.handleChange} />
				</Form.Group>
				<Form.Group className='mb-2'>
					<Button onClick={this.handleSubmit}>Connexion</Button>
				</Form.Group>
			</Form>
		);
	}
}
