import React from 'react';
import { Card } from 'react-bootstrap';
import { useNavigate } from 'react-router';
import { Buffer } from 'buffer';

class ActivityCardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: '',
    };
  }

  onClick = () => {
    let desc = '';
    let language = '';
    let schedules = [];
    const descXml = Buffer.from(this.props.descriptionXml, 'base64').toString();
    if (descXml !== '') {
      const parser = new DOMParser();
      const xml = parser.parseFromString(descXml, 'text/xml');
      desc = xml.getElementsByTagName('resume')[0].childNodes[0].nodeValue;
      language = xml.getElementsByTagName('language')[0].childNodes[0].nodeValue;
      const scheduleList = xml.getElementsByTagName('schedule')[0].children;
      for (let i = 0; i < scheduleList.length; i++) {
        let hour = scheduleList[i].children[0].textContent.split(':');
        let hourSplit = hour[0] + ':' + hour[1];
        let text = scheduleList[i].children[1].textContent;
        schedules.push(JSON.parse(JSON.stringify({ hour: hourSplit, text: text })));
      }
    }
    this.props.navigate('/activityDetails', {
      replace: false,
      state: {
        idTrip: this.props.idTrip,
        image: this.props.image,
        title: this.props.title,
        date: this.props.date,
        allDates: this.props.allDates,
        category: this.props.category,
        address: this.props.address,
        addressNumber: this.props.addressNumber,
        city: this.props.city,
        country: this.props.country,
        postcode: this.props.postcode,
        responsibleName: this.props.responsibleName,
        responsibleSurname: this.props.responsibleSurname,
        min: this.props.min,
        max: this.props.max,
        identityDocuments: this.props.identityDocuments,
        price: this.props.price,
        desc: desc,
        language: language,
        schedules: schedules,
        video: this.props.video,
        version: this.props.version,
      },
    });
  };

  render() {
    const title = this.props.title;
    const date = this.props.date;
    const category = this.props.category;
    let desc = '';
    if (this.props.descriptionXml !== '') {
      const descXml = Buffer.from(this.props.descriptionXml, 'base64').toString();
      const parser = new DOMParser();
      const xml = parser.parseFromString(descXml, 'text/xml');
      desc = xml.getElementsByTagName('resume')[0].childNodes[0].nodeValue;
    }
    return (
      <Card onClick={this.onClick} style={{ cursor: 'pointer' }} className='p-0 m-3'>
        <Card.Img variant='top' src={`data:image/jpeg;base64,${this.props.image}`} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{desc}</Card.Text>
        </Card.Body>
        <Card.Footer>
          <Card.Text as='div' className='text-muted d-flex justify-content-between'>
            <div>{date}</div>
            <div>{category}</div>
          </Card.Text>
        </Card.Footer>
      </Card>
    );
  }
}

function ActivityCard(props) {
  let navigate = useNavigate();
  return <ActivityCardComponent {...props} navigate={navigate} />;
}

export default ActivityCard;
