import React from 'react';
import { Row, Col, Form, Image } from 'react-bootstrap';

class NewActivityForm extends React.Component {
	render() {
		return (
			<div>
				<Row>
					<Col lg='7' className='mb-2'>
						<Form.Group>
							<Form.Label>Titre</Form.Label>
							{this.props.newVersion && <Form.Control type='text' value={this.props.title} disabled />}
							{!this.props.newVersion && <Form.Control type='text' name='title' onChange={this.props.handleChange} value={this.props.title} />}
						</Form.Group>
					</Col>
					<Col className='mb-2'>
						<Form.Group>
							<Form.Label>Catégorie</Form.Label>
							{this.props.newVersion && <Form.Control type='text' value={this.props.category} disabled />}
							{!this.props.newVersion && <Form.Control type='text' name='category' onChange={this.props.handleChange} value={this.props.category} />}
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col lg='7'>
						<Row className='mb-2'>
							<Form.Group as={Col}>
								<Form.Label>Min</Form.Label>
								{this.props.newVersion && <Form.Control type='number' value={this.props.min} disabled />}
								{!this.props.newVersion && <Form.Control type='number' name='min' onChange={this.props.handleChange} value={this.props.min} />}
							</Form.Group>
							<Form.Group as={Col}>
								<Form.Label>Max</Form.Label>
								{this.props.newVersion && <Form.Control type='number' value={this.props.max} disabled />}
								{!this.props.newVersion && <Form.Control type='number' name='max' onChange={this.props.handleChange} value={this.props.max} />}
							</Form.Group>
							<Form.Group as={Col}>
								<Form.Label>Prix</Form.Label>
								{this.props.newVersion && <Form.Control value={this.props.price} disabled />}
								{!this.props.newVersion && <Form.Control name='price' onChange={this.props.handleChange} value={this.props.price} />}
							</Form.Group>
						</Row>
						<Row className='mb-2'>
							<Form.Group as={Col}>
								<Form.Label>Nom du responsable</Form.Label>
								{this.props.newVersion && <Form.Control type='text' value={this.props.responsibleSurname} disabled />}
								{!this.props.newVersion && <Form.Control type='text' name='responsibleSurname' onChange={this.props.handleChange} value={this.props.responsibleSurname} />}
							</Form.Group>
							<Form.Group as={Col}>
								<Form.Label>Prénom du responsable</Form.Label>
								{this.props.newVersion && <Form.Control type='text' value={this.props.responsibleName} disabled />}
								{!this.props.newVersion && <Form.Control type='text' name='responsibleName' onChange={this.props.handleChange} value={this.props.responsibleName} />}
							</Form.Group>
						</Row>
						<Row className='mb-2'>
							<Col lg='10'>
								<Form.Group>
									<Form.Label>Adresse</Form.Label>
									{this.props.newVersion && <Form.Control type='text' value={this.props.address} disabled />}
									{!this.props.newVersion && <Form.Control type='text' name='address' onChange={this.props.handleChange} value={this.props.address} />}
								</Form.Group>
							</Col>
							<Col>
								<Form.Group>
									<Form.Label>Numéro</Form.Label>
									{this.props.newVersion && <Form.Control type='number' value={this.props.addressNumber} disabled />}
									{!this.props.newVersion && <Form.Control type='number' name='addressNumber' onChange={this.props.handleChange} value={this.props.addressNumber} />}
								</Form.Group>
							</Col>
						</Row>
						<Row className='mb-2'>
							<Form.Group as={Col}>
								<Form.Label>Ville</Form.Label>
								{this.props.newVersion && <Form.Control type='text' value={this.props.city} disabled />}
								{!this.props.newVersion && <Form.Control type='text' name='city' onChange={this.props.handleChange} value={this.props.city} />}
							</Form.Group>
							<Form.Group as={Col}>
								<Form.Label>NPA</Form.Label>
								{this.props.newVersion && <Form.Control type='number' value={this.props.postcode} disabled />}
								{!this.props.newVersion && <Form.Control type='number' name='postcode' onChange={this.props.handleChange} value={this.props.postcode} />}
							</Form.Group>
							<Form.Group as={Col}>
								<Form.Label>Pays</Form.Label>
								{this.props.newVersion && <Form.Control type='text' value={this.props.country} disabled />}
								{!this.props.newVersion && <Form.Control type='text' name='country' onChange={this.props.handleChange} value={this.props.country} />}
							</Form.Group>
						</Row>
					</Col>
					<Col className='mb-2'>
						<Form.Label>Image</Form.Label>
						{this.props.newVersion && (
							<div>
								<Row className='mb-1'>
									<Form.Group>
										<Image src={this.props.image} thumbnail />
									</Form.Group>
								</Row>
								<Row>
									<Form.Group>
										<Form.Control type='file' disabled />
									</Form.Group>
								</Row>
							</div>
						)}
						{!this.props.newVersion && (
							<div>
								<Row className='mb-1'>
									<Form.Group>
										<Image src={this.props.imageUrl} thumbnail />
									</Form.Group>
								</Row>
								<Row>
									<Form.Group>
										<Form.Control type='file' name='image' onChange={this.props.handleImageChange} />
									</Form.Group>
								</Row>
							</div>
						)}
					</Col>
				</Row>
				<Row className='mb-2'>
					<Form.Group className='mb-2'>
						<Form.Label>URL vidéo</Form.Label>
						{this.props.newVersion && <Form.Control type='text' value={this.props.video} disabled />}
						{!this.props.newVersion && <Form.Control type='text' name='video' onChange={this.props.handleChange} value={this.props.video} />}
					</Form.Group>
				</Row>
				<Row className='mb-2'>
					<Form.Group>
						<Form.Label>Document(s) nécessaire : chaque ligne représente un nouveau document nécessaire</Form.Label>
						{this.props.newVersion && (
							<Form.Control type='text' as='textarea' rows={this.props.identityDocumentNumber} placeholder='passeport' value={this.props.identityDocument} disabled />
						)}
						{!this.props.newVersion && (
							<Form.Control
								type='text'
								as='textarea'
								name='identityDocument'
								onChange={this.props.handleChange}
								rows={this.props.identityDocumentNumber}
								placeholder='passeport'
								value={this.props.identityDocument}
							/>
						)}
					</Form.Group>
				</Row>
				<Row className='mb-2'>
					<Form.Group>
						<Form.Label>Dates : chaque ligne représente une nouvelle date de l'activité</Form.Label>
						{this.props.newVersion && <Form.Control type='text' as='textarea' rows={this.props.dateNumber} placeholder='jj-mm-yyyy' value={this.props.date} disabled />}
						{!this.props.newVersion && (
							<Form.Control type='text' as='textarea' name='date' onChange={this.props.handleChange} rows={this.props.dateNumber} placeholder='jj-mm-yyyy' value={this.props.date} />
						)}
					</Form.Group>
				</Row>
				<Row className='mb-2'>
					<Form.Group>
						<Form.Label>Langue</Form.Label>
						{this.props.newVersion && <Form.Control type='text' value={this.props.language} disabled />}
						{!this.props.newVersion && <Form.Control type='text' name='language' onChange={this.props.handleChange} value={this.props.language} />}
					</Form.Group>
				</Row>
				<Row className='mb-2'>
					<Form.Group>
						<Form.Label>Description</Form.Label>
						{this.props.newVersion && <Form.Control type='text' as='textarea' rows={3} value={this.props.desc} disabled />}
						{!this.props.newVersion && <Form.Control type='text' as='textarea' name='desc' onChange={this.props.handleChange} rows={3} value={this.props.desc} />}
					</Form.Group>
				</Row>
				<Row className='mb-2'>
					<Form.Group>
						<Form.Label>Programme : chaque ligne représente une nouvelle entrée du programme de l'activité</Form.Label>
						{this.props.newVersion && (
							<Form.Control type='text' as='textarea' rows={this.props.scheduleNumber} placeholder="hh:mm, description de l'horaire" value={this.props.schedule} disabled />
						)}
						{!this.props.newVersion && (
							<Form.Control
								type='text'
								as='textarea'
								name='schedule'
								onChange={this.props.handleChange}
								rows={this.props.scheduleNumber}
								placeholder="hh:mm, description de l'horaire"
								value={this.props.schedule}
							/>
						)}
					</Form.Group>
				</Row>
			</div>
		);
	}
}

export default NewActivityForm;
