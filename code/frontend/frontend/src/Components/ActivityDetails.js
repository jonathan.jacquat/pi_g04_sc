import React from 'react';
import { Container, Image, Row, Col, Button, Form, Modal, Carousel } from 'react-bootstrap';
import { useNavigate, useLocation } from 'react-router';
import EnrollmentForm from './EnrollmentForm';
import UnenrollmentForm from './UnenrollmentForm';
import Login from './Login';
import ReactPlayer from 'react-player';
import EnrollList from './EnrollList';

class ActivityDetailsComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showEnroll: false,
			showEnrollError: false,
			showUnenroll: false,
			showEnrollList: false,
			unenroll: false,
			enrolledList: [],
			userList: [],
		};
		this.handleEnroll = this.handleEnroll.bind(this);
		this.handleUnenroll = this.handleUnenroll.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
		this.handleDuplicate = this.handleDuplicate.bind(this);
		this.handleModify = this.handleModify.bind(this);
		this.handleModalEnrollList = this.handleModalEnrollList.bind(this);
	}

	prepareDate() {
		const sepDate = this.props.location.date.split('.');
		return sepDate[2] + '-' + sepDate[1] + '-' + sepDate[0];
	}

	componentDidMount() {
		this.fetchRegistered();
		this.fetchUserList();
	}

	fetchRegistered() {
		if (this.props.userEmail && this.props.userRole !== 'admin') {
			const requestOptions = {
				method: 'GET',
				headers: { 'Content-Type': 'application/json', token: this.props.token },
			};
			fetch('http://localhost:8080/trip/getRegisteredByUser/' + this.props.userEmail + '/' + this.props.location.idTrip + '/' + this.prepareDate(), requestOptions)
				.then((res) => res.json())
				.then((json) => {
					if (json.length > 0) {
						this.setState({ unenroll: true, enrolledList: json });
					} else {
						if (this.state.enrolledList.length > 0) {
							this.setState({ unenroll: false, enrolledList: [] });
						}
					}
				});
		}
	}

	fetchUserList() {
		if (this.props.userRole === 'admin') {
			const requestOptions = {
				method: 'GET',
				headers: { 'Content-Type': 'application/json', token: this.props.token },
			};
			fetch('http://localhost:8080/trip/getRegistrationByTrip/' + this.props.location.idTrip + '/' + this.prepareDate(), requestOptions)
				.then((res) => res.json())
				.then((json) => {
					this.setState({ userList: json });
				});
		}
	}

	componentDidUpdate(prevState) {
		if (this.props.userConnected !== prevState.userConnected) {
			this.fetchRegistered();
			this.fetchUserList();
		}
	}

	goBack = () => {
		this.props.navigate(-1);
	};

	handleModalEnroll() {
		this.setState({ showEnroll: !this.state.showEnroll, showEnrollError: false });
	}

	handleModalUnenroll() {
		this.setState({ showUnenroll: !this.state.showUnenroll });
	}

	handleModalEnrollList() {
		this.setState({ showEnrollList: !this.state.showEnrollList });
	}

	handleLogin(email, password) {
		this.setState({ showLogin: false });
		this.props.handleLogin(email, password);
	}

	handleEnroll(enrollList) {
		const idTrip = this.props.location.idTrip;
		const registerer = this.props.userEmail;

		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', token: this.props.token },
			body: JSON.stringify({ clients: enrollList, idTrip: idTrip, tripDate: this.prepareDate(), registerer: registerer }),
		};
		fetch('http://localhost:8080/trip/register', requestOptions).then((res) => {
			if (res.status === 200) {
				this.fetchRegistered();
				this.handleModalEnroll();
			} else {
				this.setState({ showEnrollError: true });
			}
		});
	}

	handleUnenroll(toUnenroll) {
		if (toUnenroll.length > 0) {
			const idTrip = this.props.location.idTrip;
			const requestOptions = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', token: this.props.token },
				body: JSON.stringify({ clients: toUnenroll, idTrip: idTrip, tripDate: this.prepareDate(), registerer: 'egal' }),
			};
			fetch('http://localhost:8080/trip/unregister', requestOptions)
				.then((res) => res)
				.then((res) => {
					if (res.status === 200) {
						this.fetchRegistered();
						this.handleModalUnenroll();
					} else {
						console.log(res.text());
					}
				});
		}
	}

	handleDuplicate() {
		const loc = this.props.location;
		this.props.navigate('/addActivity', {
			state: {
				duplicate: true,
				title: loc.title,
				category: loc.category,
				min: loc.min,
				max: loc.max,
				price: loc.price,
				responsibleName: loc.responsibleName,
				responsibleSurname: loc.responsibleSurname,
				address: loc.address,
				addressNumber: loc.addressNumber,
				city: loc.city,
				postcode: loc.postcode,
				country: loc.country,
				image: loc.image,
				identityDocument: loc.identityDocuments,
				date: loc.allDates,
				language: loc.language,
				desc: loc.desc,
				schedule: loc.schedules,
				imageUrl: loc.imageUrl,
				video: loc.video,
				modify: false,
				version: loc.version,
			},
		});
	}

	handleModify() {
		const loc = this.props.location;
		this.props.navigate('/addActivity', {
			state: {
				duplicate: false,
				idTrip: loc.idTrip,
				title: loc.title,
				category: loc.category,
				min: loc.min,
				max: loc.max,
				price: loc.price,
				responsibleName: loc.responsibleName,
				responsibleSurname: loc.responsibleSurname,
				address: loc.address,
				addressNumber: loc.addressNumber,
				city: loc.city,
				postcode: loc.postcode,
				country: loc.country,
				image: loc.image,
				identityDocument: loc.identityDocuments,
				date: loc.allDates,
				selectedDate: loc.date,
				language: loc.language,
				desc: loc.desc,
				schedule: loc.schedules,
				imageUrl: loc.imageUrl,
				video: loc.video,
				modify: true,
				version: loc.version,
			},
		});
	}

	render() {
		const userConnected = this.props.userConnected;
		const userRole = this.props.userRole;
		const loc = this.props.location;
		const img = loc.image;
		const title = loc.title;
		const category = loc.category;
		const description = loc.desc;
		const date = loc.date;
		const address = loc.address;
		const localisation = loc.postcode + ' ' + loc.city + ', ' + loc.country;
		const responsibleName = loc.responsibleName;
		const responsibleSurname = loc.responsibleSurname;
		const min = loc.min;
		const max = loc.max;
		const identityDocuments = loc.identityDocuments;
		const price = loc.price;
		const language = loc.language;
		const schedules = loc.schedules;
		const video = loc.video;
		return (
			<div>
				<Container>
					<Form>
						<Row className='mt-2 mb-2'>
							<Col sm='2' lg='5'>
								<Button variant='outline-secondary' onClick={this.goBack}>
									Retour
								</Button>
							</Col>
							<Col sm='10' lg='7'>
								<Row>
									<div className='d-flex justify-content-between'>
										<div className='fs-3'>
											<strong>{title}</strong>
										</div>
										<div className='fs-4 text-muted'>{category}</div>
									</div>
								</Row>
							</Col>
						</Row>
						<Row>
							<Col sm='12' lg='5' className='mb-2'>
								<Carousel interval={null}>
									<Carousel.Item>
										<Image src={`data:image/jpeg;base64,${img}`} thumbnail />
									</Carousel.Item>
									<Carousel.Item>
										<ReactPlayer url={video} volume={0.0} width='100%' height='100%' />
									</Carousel.Item>
								</Carousel>
							</Col>
							<Col sm='12' lg='7' className='mb-2 fs-6'>
								<Row className='text-justify'>
									<div>{description}</div>
								</Row>
								<Row>
									<Col sm='12' lg='6'>
										<Row className='mt-1 mb-1 fs-6'>
											<div>
												<strong>Date :</strong> {date}
											</div>
										</Row>
										<Row className='mt-1 mb-1 fs-6'>
											<div>
												<strong>Lieu :</strong> {address}, {localisation}
											</div>
										</Row>
										<Row className='mt-1 mb-1 fs-6'>
											<div>
												<strong>Responsable :</strong> {responsibleName} {responsibleSurname}
											</div>
										</Row>
										<Row className='mt-1 mb-1 fs-6'>
											<div>
												<strong>Nombre de personne :</strong> {min} min. / {max} max.
											</div>
										</Row>
										<Row className='mt-1 mb-1 fs-6'>
											<div>
												<strong>Papier(s) nécessaire :</strong>
												<ul>
													{identityDocuments.map((document, index) => (
														<li key={index}>{document.name}</li>
													))}
												</ul>
											</div>
										</Row>
										<Row className='mt-1 mb-1 fs-6'>
											<div>
												<strong>Langue :</strong> {language}
											</div>
										</Row>
									</Col>
									<Col sm='12' md='6' className='mt-1 mb-1 fs-6'>
										<div>
											<strong>Détails de l'activité :</strong>
											<ul>
												{schedules.map((schedule, index) => (
													<li key={index}>
														{schedule.hour} : {schedule.text}
													</li>
												))}
											</ul>
										</div>
									</Col>
								</Row>
								<Row>
									<Col>
										<Row className='mt-4 fs-6 text-end'>
											<div>
												<strong>Prix :</strong> {price.toFixed(2)} CHF
											</div>
										</Row>
										{!userConnected && (
											<Row className='mt-3 mb-3'>
												<Button variant='success' onClick={() => this.props.handleModalLogin()}>
													Connexion
												</Button>
											</Row>
										)}
										{userConnected && userRole === 'client' && (
											<div>
												{this.state.unenroll && (
													<Row className='ml-1 mt-2 mb-2'>
														<Col className='m-1'>
															<Row>
																<Button variant='dark' onClick={() => this.handleModalEnroll()}>
																	Inscription
																</Button>
															</Row>
														</Col>
														<Col className='m-1'>
															<Row>
																<Button variant='outline-dark' onClick={() => this.handleModalUnenroll()}>
																	Désinscription
																</Button>
															</Row>
														</Col>
													</Row>
												)}
												{!this.state.unenroll && (
													<Row className='ml-1 mt-3 mb-3'>
														<Button
															variant='dark'
															onClick={() => {
																this.handleModalEnroll();
															}}
														>
															Inscription
														</Button>
													</Row>
												)}
											</div>
										)}
										{userConnected && userRole === 'admin' && this.state.userList.length > 0 && (
											<Row className='mt-3 mb-3'>
												<Col>
													<Button variant='secondary' style={{ width: '100%' }} onClick={this.handleModify} disabled>
														Modifier
													</Button>
												</Col>
												<Col>
													<Button variant='secondary' style={{ width: '100%' }} onClick={this.handleDuplicate}>
														Dupliquer
													</Button>
												</Col>
												<Col>
													<Button variant='secondary' style={{ width: '100%' }} onClick={() => this.handleModalEnrollList()}>
														Liste des inscription
													</Button>
												</Col>
											</Row>
										)}
										{userConnected && userRole === 'admin' && this.state.userList.length === 0 && (
											<Row className='mt-3 mb-3'>
												<Col>
													<Button variant='secondary' style={{ width: '100%' }} onClick={this.handleModify}>
														Modifier
													</Button>
												</Col>
												<Col>
													<Button variant='secondary' style={{ width: '100%' }} onClick={this.handleDuplicate}>
														Dupliquer
													</Button>
												</Col>
												<Col>
													<Button variant='secondary' style={{ width: '100%' }} onClick={() => this.handleModalEnrollList()} disabled>
														Liste des inscription
													</Button>
												</Col>
											</Row>
										)}
									</Col>
								</Row>
							</Col>
						</Row>
					</Form>
				</Container>
				<Modal show={this.props.showModalLogin} onHide={() => this.props.handleModalLogin()}>
					<Modal.Body>
						<Login handleLogin={this.handleLogin} showLoginError={this.props.showLoginError} />
					</Modal.Body>
				</Modal>
				<Modal show={this.state.showEnroll} onHide={() => this.handleModalEnroll()}>
					<Modal.Body>
						<EnrollmentForm handleEnroll={this.handleEnroll} showEnrollError={this.state.showEnrollError} />
					</Modal.Body>
				</Modal>
				<Modal show={this.state.showUnenroll} onHide={() => this.handleModalUnenroll()}>
					<Modal.Body>
						<UnenrollmentForm handleUnenroll={this.handleUnenroll} enrolledList={this.state.enrolledList} />
					</Modal.Body>
				</Modal>
				<Modal show={this.state.showEnrollList} onHide={() => this.handleModalEnrollList()}>
					<Modal.Body>
						<EnrollList
							enrollList={this.state.userList}
							title={this.props.location.title}
							date={this.props.location.date}
							category={this.props.location.category}
							language={this.props.location.language}
							price={this.props.location.price}
							handleModalEnrollList={this.handleModalEnrollList}
						/>
					</Modal.Body>
				</Modal>
			</div>
		);
	}
}

function ActivityDetails(props) {
	const { state } = useLocation();
	let navigate = useNavigate();

	return <ActivityDetailsComponent {...props} location={state} navigate={navigate} />;
}

export default ActivityDetails;
