import React from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import md5 from 'md5';
import Navigation from './Components/Navigation';
import ActivityList from './Components/ActivityList';
import ActivityDetails from './Components/ActivityDetails';
import NewActivity from './Components/NewActivity';
import MyActivity from './Components/MyActivity';
import UserList from './Components/UserList';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			userConnected: false,
			userRole: '',
			userEmail: '',
			token: '',
			showLogin: false,
			showLoginError: false,
		};
		this.login = this.login.bind(this);
		this.logout = this.logout.bind(this);
		this.handleModalLogin = this.handleModalLogin.bind(this);
	}

	login(email, password) {
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ email: email, password: md5(password) }),
		};
		fetch('http://localhost:8080/account/login', requestOptions).then((res) => {
			if (res.status === 200) {
				res.json().then((json) => {
					this.setState({
						userRole: json.user.role,
						userConnected: true,
						userEmail: json.user.email,
						token: json.token,
						showLogin: false,
						showLoginError: false,
					});
				});
			} else {
				this.setState({ showLoginError: true });
				console.log('erreur de login');
			}
		});
	}

	logout() {
		this.setState({ userRole: '', userConnected: false, userEmail: '', token: '' });
	}

	handleModalLogin() {
		this.setState({ showLogin: !this.state.showLogin, showLoginError: false });
	}

	render() {
		return (
			<div>
				<Router>
					<Navigation
						userConnected={this.state.userConnected}
						userRole={this.state.userRole}
						handleLogin={this.login}
						handleLogout={this.logout}
						showModalLogin={this.state.showLogin}
						handleModalLogin={this.handleModalLogin}
						showLoginError={this.state.showLoginError}
					/>
					<Routes>
						<Route exact path='/' element={<ActivityList userConnected={this.state.userConnected} userRole={this.state.userRole} token={this.state.token} />} />
						<Route
							path='/activityDetails'
							element={
								<ActivityDetails
									userConnected={this.state.userConnected}
									userRole={this.state.userRole}
									userEmail={this.state.userEmail}
									token={this.state.token}
									handleLogin={this.login}
									showModalLogin={this.state.showLogin}
									handleModalLogin={this.handleModalLogin}
									showLoginError={this.state.showLoginError}
								/>
							}
						/>
						<Route path='/addActivity' element={<NewActivity userConnected={this.state.userConnected} userRole={this.state.userRole} token={this.state.token} />} />
						<Route
							path='/myActivities'
							element={<MyActivity userConnected={this.state.userConnected} userRole={this.state.userRole} userEmail={this.state.userEmail} token={this.state.token} />}
						/>
						<Route path='/userList' element={<UserList userConnected={this.state.userConnected} userRole={this.state.userRole} token={this.state.token} />} />
					</Routes>
				</Router>
			</div>
		);
	}
}

export default App;
