package pi.g4.sorties_culturelles.authorization;

public enum Role {

    ANONYMOUS("anonymous"), CLIENT("client"), ADMIN("admin");
    private final String text;

    Role(String text) {
        this.text = text;
    }

    public static Role getRoleByString(String t) {
        for (Role role : Role.values())
            if (role.text.equals(t))
                return role;
        return null;
    }
}
