package pi.g4.sorties_culturelles.model;

import javax.persistence.*;

@Entity
@Table(name = "identity_document", schema = "public", catalog = "sc_db")
public class IdentityDocument {
    @Id
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentityDocument that = (IdentityDocument) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
