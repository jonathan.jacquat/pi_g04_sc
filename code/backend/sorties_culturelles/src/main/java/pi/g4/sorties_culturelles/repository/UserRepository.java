package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pi.g4.sorties_culturelles.model.Person;

public interface UserRepository extends JpaRepository<Person, String> {

    @Query(nativeQuery = true, value ="SELECT email, first_name, last_name, user_role FROM pr_get_role(:email, :password)")
    Object[] getUser(@Param("email") String email, @Param("password") String password);
}
