package pi.g4.sorties_culturelles.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(RegistrationPK.class)
public class Registration {

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_trip", referencedColumnName="id_trip", nullable = false)
    private Trip trip;

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "email_client", referencedColumnName="email", nullable = false)
    private Client client;

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "trip_date", referencedColumnName="trip_date", nullable = false)
    private TripDate tripDate;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "email_register", referencedColumnName="email", nullable = false)
    private Client registerer;


    public Registration(Client client, Trip trip, TripDate tripDate, Client registerer) {
        this.client = client;
        this.trip = trip;
        this.tripDate = tripDate;
        this.registerer = registerer;
    }

    public Registration() {
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public TripDate getTripDate() {
        return tripDate;
    }

    public void setTripDate(TripDate tripDate) {
        this.tripDate = tripDate;
    }

    public Client getRegisterer() {
        return registerer;
    }

    public void setRegisterer(Client registerer) {
        this.registerer = registerer;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registration that = (Registration) o;
        return client.equals(that.client) && trip.equals(that.trip) && tripDate.equals(that.tripDate) && registerer.equals(that.registerer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, trip, tripDate, registerer);
    }
}
