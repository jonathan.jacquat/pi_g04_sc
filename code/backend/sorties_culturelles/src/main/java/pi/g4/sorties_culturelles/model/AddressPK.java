package pi.g4.sorties_culturelles.model;

import javax.persistence.*;
import java.io.Serializable;

public class AddressPK implements Serializable {

    @Column(name = "address", nullable = false, length = 50)
    @Id
    private String address;

    @Column(name = "address_number", nullable = false)
    @Id
    private int addressNumber;

    public AddressPK() {

    }

    public AddressPK(String address, int addressNumber) {
        this.address = address;
        this.addressNumber = addressNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(int addressNumber) {
        this.addressNumber = addressNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressPK addressPK = (AddressPK) o;

        if (addressNumber != addressPK.addressNumber) return false;
        if (address != null ? !address.equals(addressPK.address) : addressPK.address != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + addressNumber;
        return result;
    }
}
