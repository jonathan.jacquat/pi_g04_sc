package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pi.g4.sorties_culturelles.model.Registration;
import pi.g4.sorties_culturelles.model.RegistrationPK;

import java.util.Date;
import java.util.List;


public interface RegistrationRepository extends JpaRepository<Registration, RegistrationPK> {

   @Query("SELECT count(r.client) FROM Registration r WHERE r.trip.idTrip = :trip_id AND r.tripDate.tripDate = :trip_date")
   int getNbRegistration(int trip_id, Date trip_date);

   @Query("SELECT r FROM Registration r WHERE r.client.email = :email")
   List<Registration> getRegistrationByEmail(String email);

   @Query("SELECT r FROM Registration r WHERE r.trip.idTrip = :id AND r.tripDate.tripDate = :date")
   List<Registration> getRegistrationByTrip(int id, Date date);

   @Query("SELECT r FROM Registration r WHERE (r.registerer.email = :email OR r.client.email = :email) AND r.trip.idTrip = :tripId AND r.tripDate.tripDate = :date")
   List<Registration> getRegisteredByUser(String email, int tripId, Date date);
}
