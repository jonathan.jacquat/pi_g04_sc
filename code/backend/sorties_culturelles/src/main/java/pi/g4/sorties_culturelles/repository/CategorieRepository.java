package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pi.g4.sorties_culturelles.model.Categorie;

public interface CategorieRepository extends JpaRepository<Categorie, String> {

}
