package pi.g4.sorties_culturelles.datasources;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@ComponentScan({"pi.g4.sorties_culturelles"})
@EntityScan(basePackages = "pi.g4.sorties_culturelles")
@EnableJpaRepositories(basePackages = "pi.g4.sorties_culturelles")
public class DataSourcesConfig {
    /**
     * Defines the data source for the application
     *
     * @return
     */
    @Bean
    @ConfigurationProperties(
            prefix = "spring.datasource"
    )
    public DataSource dataSource() {
        Map<Object, Object> resolvedDataSources = new HashMap<>();

        resolvedDataSources.put("ADMIN", newDataSourceAdmin());
        resolvedDataSources.put("CLIENT", newDataSourceClient());
        resolvedDataSources.put("ANONYMOUS", newDataSourceAnonymous());

        MultiTenantDataSource dataSource = new MultiTenantDataSource();
        dataSource.setDefaultTargetDataSource(newDataSourceScAdmin());
        dataSource.setTargetDataSources(resolvedDataSources);

        return dataSource;
    }

    @Primary
    @Bean(name = "dsAnonymous")
    @ConfigurationProperties(prefix = "spring.datasource.anonymous")
    public DataSource newDataSourceAnonymous() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dsClient")
    @ConfigurationProperties(prefix = "spring.datasource.client")
    public DataSource newDataSourceClient() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dsAdmin")
    @ConfigurationProperties(prefix = "spring.datasource.admin")
    public DataSource newDataSourceAdmin() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dsScAdmin")
    @ConfigurationProperties(prefix = "spring.datasource.scadmin")
    public DataSource newDataSourceScAdmin() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean multiEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("pi.g4.sorties_culturelles");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        return em;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager multiTransactionManager() {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                multiEntityManager().getObject());
        return transactionManager;
    }

}
