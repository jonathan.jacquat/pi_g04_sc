package pi.g4.sorties_culturelles.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.jose.JoseHeader;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.stereotype.Service;
import pi.g4.sorties_culturelles.authorization.Role;
import pi.g4.sorties_culturelles.datasources.TenantContext;
import pi.g4.sorties_culturelles.model.UserLogin;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthenticationService {

    @Autowired
    private JwtEncoder jwtEncoder;

    @Autowired
    private JwtDecoder jwtDecoder;

    public Map<String, Object> validateToken(String token, Role requiredRole) {
        Map<String, Object> map = new HashMap<>();

        if (requiredRole == Role.ANONYMOUS) {
            TenantContext.setCurrentTenant(Role.getRoleByString("anonymous"));
            return map;
        }

        try {
            Jwt jwt = jwtDecoder.decode(token);
            map.put("jwt", jwt);
            System.out.println(jwt.getClaims());
            String userEmail = (String) jwt.getClaims().get("user_email");
            map.put("userEmail", userEmail);
            String userRole = (String) jwt.getClaims().get("user_role");
            Role role = Role.getRoleByString(userRole);
            map.put("role", role);

            if (requiredRole == null) {
                TenantContext.setCurrentTenant(Role.getRoleByString("anonymous"));
            } else if (requiredRole == role) {
                TenantContext.setCurrentTenant(role);
            } else {
                map.put("responseEntity", ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Role not authorized"));
            }

        } catch (BadJwtException e) {
            map.put("responseEntity", ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid JWT token"));
        }
        return map;
    }

    public Jwt createNewJwt(UserLogin user) {
        JoseHeader joseHeader = joseHeader().build();
        JwtClaimsSet jwtClaimsSet = jwtClaimsSet()
                .claim("user_role", user.getRole())
                .claim("user_first_name", user.getFirstName())
                .claim("user_last_name", user.getLastName())
                .claim("user_email", user.getEmail())
                .build();
        return jwtEncoder.encode(joseHeader, jwtClaimsSet);
    }

    private static JwtClaimsSet.Builder jwtClaimsSet() {
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plus(1, ChronoUnit.DAYS);

        return JwtClaimsSet.withClaims()
                .issuedAt(issuedAt)
                .notBefore(issuedAt)
                .expiresAt(expiresAt)
                .id("jti");
    }

    private static JoseHeader.Builder joseHeader() {
        Map<String, Object> rsaJwk = new HashMap<>();
        rsaJwk.put("kty", "RSA");
        rsaJwk.put("n", "modulus");
        rsaJwk.put("e", "exponent");

        return JoseHeader.withAlgorithm(SignatureAlgorithm.RS256)
                .type("JWT")
                .jwk(rsaJwk)
                .keyId("keyId")
                .contentType("jwt-content-type");
    }
}
