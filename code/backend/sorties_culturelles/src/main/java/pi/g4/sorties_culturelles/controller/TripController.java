package pi.g4.sorties_culturelles.controller;

import org.hibernate.exception.GenericJDBCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.web.bind.annotation.*;
import pi.g4.sorties_culturelles.authorization.Role;
import pi.g4.sorties_culturelles.model.*;
import pi.g4.sorties_culturelles.service.AuthenticationService;
import pi.g4.sorties_culturelles.service.RegistrationService;
import pi.g4.sorties_culturelles.service.TripService;
import pi.g4.sorties_culturelles.tools.XMLValidator;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/trip")
public class TripController {

    @Autowired
    private TripService tripService;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private AuthenticationService authenticationService;

    Logger logger = LoggerFactory.getLogger(TripController.class);


    /**
     * Permet de créer un trip.
     *
     * @param token JWT token de l'utilisateur.
     * @param trip Un objet JSON contenant les infos du trip.
     * @return 200 avec un JSON contenant les informations du trip créé,
     * 400 si une erreur s'est produite durant la création
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @PostMapping(value = "/create", produces = "application/json")
    public ResponseEntity createTrip(@RequestHeader String token, @RequestBody Trip trip) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        logger.debug("Creation of trip " + trip);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        try {
            if (!XMLValidator.validateSchema(trip.getDescriptionXml())) {
                logger.debug("Error with the XML");
                return ResponseEntity.badRequest().body("The XML is invalid");
            }
        } catch (ParserConfigurationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Error during XML check");
        } catch (IOException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Error during XML check");
        }

        try {
            trip = tripService.createTrip(trip);
            logger.debug("creation success");
            return ResponseEntity.ok(trip);

        } catch (DataIntegrityViolationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("The action violates some constraints");
        }
    }

    /**
     * Permet de modifier un trip.
     *
     * @param token JWT token de l'utilisateur.
     * @param trip Un JSON contenant le trip modifié.
     * @return 200 avec un JSON contenant les informations du trip modifié,
     * 400 si une erreur s'est produite durant la modification,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé,
     * 422 si une erreur de concurrance est survenue.
     */
    @PutMapping(value = "/update", produces = "application/json")
    public ResponseEntity updateTrip(@RequestHeader String token, @RequestBody Trip trip) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        logger.debug("Update for the trip " + trip);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        Trip old = tripService.getTrip(trip.getIdTrip());
        if (old == null) {
            logger.error("Trip id " + trip.getIdTrip() + " does not exist during modify");
            return ResponseEntity.badRequest().body("This tripId does not exist");
        }

        try {
            if (!XMLValidator.validateSchema(trip.getDescriptionXml())) {
                logger.debug("XML error");
                return ResponseEntity.badRequest().body("The XML is invalid");
            }
        } catch (ParserConfigurationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Error during XML check");
        } catch (IOException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Error during XML check");
        }

        try {
            trip = tripService.updateTrip(trip);
            trip.setVersion(trip.getVersion()+1);
            return ResponseEntity.ok(trip);

        } catch (JpaSystemException e) {
            if (e.getCause() instanceof GenericJDBCException) {
                GenericJDBCException gje = (GenericJDBCException) e.getCause();
                String exceptionMessage = gje.getCause().getMessage();
                if (exceptionMessage != null && exceptionMessage.contains("The version number does not match")) {
                    trip = tripService.getTrip(trip.getIdTrip());
                    return ResponseEntity.unprocessableEntity().body(trip);
                }
            }
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (DataIntegrityViolationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("The action violates some constraints");
        } catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    /**
     * Permet de récupérer la totalité des trip.
     *
     * @return 200 avec un JSON contenant les informations des trip,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/getAll", produces = "application/json")
    public ResponseEntity getAll() {

        // Used to set the tenant to "anonymous" so the "anonymous" datasource is used
        authenticationService.validateToken("", Role.ANONYMOUS);

        logger.debug("Récupération des trips");

        return ResponseEntity.ok(tripService.getAllTrips());
    }


    /**
     * Permet de récupérer les trip validés.
     *
     * @param token JWT token de l'utilisateur.
     * @return 200 avec un JSON la liste des trip validé
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/getValidate", produces = "application/json")
    public ResponseEntity getValidate(@RequestHeader String token) {
        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        logger.debug("Récupéation des sorties valides");

        return ResponseEntity.ok(tripService.getTripsValidate());
    }


    /**
     * Permet à un utilisateur de s'enregistrer à un trip.
     *
     * @param token JWT token de l'utilisateur.
     * @param registrations JSON contenant les informations sur l'enregistrement.
     * @return 200 avec un JSON contenant les informations de l'enregistrement,
     * 400 si une erreur s'est produite durant l'enregistrement,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @PostMapping(value = "/register", produces = "application/json")
    public ResponseEntity register(@RequestHeader String token, @RequestBody RegistrationList registrations) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.CLIENT);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        logger.debug("Enregistrement à une sortie" + registrations);

        try {
            List<Registration> registrationList = registrationService.register(registrations);
            return ResponseEntity.ok(registrationList);

        } catch (DataIntegrityViolationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("The action violates some constraints");
        } catch (JpaSystemException e) {
            if (e.getCause() instanceof GenericJDBCException) {
                GenericJDBCException gje = (GenericJDBCException) e.getCause();
                String exceptionMessage = gje.getCause().getMessage();
                if (exceptionMessage != null && exceptionMessage.contains("Trip is in less than 7 days"))
                    return ResponseEntity.badRequest().body("Trip is in less than 7 days");
            }
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    /**
     * Permet à l'utilisateur de se désinscrire d'un trip.
     *
     * @param token JWT token de l'utilisateur.
     * @param registrations JSON contenant les informations de désinscription.
     * 200 avec un message sécifiant que cela s'est bien passé,
     * 400 si une erreur s'est produite durant la désinscription,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @PostMapping(value = "/unregister", produces = "application/json")
    public ResponseEntity unregister(@RequestHeader String token, @RequestBody RegistrationList registrations) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.CLIENT);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        logger.debug("Descinscription " + registrations);

        try {
            registrationService.unregister(registrations);
            return ResponseEntity.ok("Unregister success");

        } catch (DataIntegrityViolationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("The action violates some constraints");
        } catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    /**
     * Permet à l'utilisateur de récupérer les inscription à un trip.
     *
     * @param token JWT token de l'utilisateur.
     * @param tripId ID du trip concerné.
     * @param tripDate Date du trip concerné.
     * 200 avec un JSON contenant les inscriptions au trip,
     * 400 si une erreur s'est produite durant la récupération des informations,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/getRegistrationByTrip/{tripId}/{tripDate}", produces = "application/json")
    public ResponseEntity getRegistrationByTrip(@RequestHeader String token, @PathVariable(name = "tripId") int tripId, @PathVariable(name = "tripDate") String tripDate) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        logger.debug("getRegistrationByTrip " + tripDate + " " + tripId);

        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(tripDate);
        } catch (ParseException e) {
            logger.debug(e.toString());
            return ResponseEntity.badRequest().body("Bad date format");
        }

        List<Registration> registrations = registrationService.getRegistrationByTrip(tripId, date);

        List<String> emails = new ArrayList<>();

        for (Registration registration : registrations) {
            emails.add(registration.getClient().getPerson().getEmail());
        }

        return ResponseEntity.ok(emails);
    }

    /**
     * Permet de récupérer les inscriptions d'un utilisateur.
     *
     * @param token JWT token de l'utilisateur.
     * @param email L'email de l'utilisateur concerné.
     * 200 avec un JSON contenant les sorties de l'utilisateur,
     * 400 si une erreur s'est produite durant la récupération des informations,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/getRegistration/{email}", produces = "application/json")
    public ResponseEntity getRegistrationByEmail(@RequestHeader String token, @PathVariable(name = "email") String email) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        if (map.containsKey("responseEntity"))
            map = authenticationService.validateToken(token, Role.CLIENT);
                if (map.containsKey("responseEntity"))
                    return (ResponseEntity) map.get("responseEntity");

        logger.debug("getRegistration " + email);


        if (map.containsKey("role") && map.get("role") == Role.CLIENT && !email.equals(map.get("userEmail"))) {
            logger.warn("User " + map.get("userEmail") + " try to get registration of " + email);
            return ResponseEntity.badRequest().body("Client not authorized");
        }


        List<Registration> registrations = registrationService.getRegistrationByEmail(email);

        List<RegistrationList> registrationList = new ArrayList<>();

        for (Registration registration : registrations) {
            ArrayList<String> client = new ArrayList<>();
            client.add(registration.getClient().getPerson().getEmail());
            registrationList.add(new RegistrationList(client, registration.getTrip().getIdTrip(), registration.getTripDate().getTripDate(), registration.getRegisterer().getPerson().getEmail()));
        }
        return ResponseEntity.ok(registrationList);
    }

    /**
     * Permet de récupérer l'utilisateur qui a inscrit une certaine personne.
     *
     * @param token JWT token de l'utilisateur.
     * @param email L'email de l'utilisateur concerné.
     * @param tripId Id du trip concerné.
     * @param tripDate Date du trip concerné.
     * 200 avec un JSON contenant la personne qui a fait l'inscription,
     * 400 si une erreur s'est produite durant la récupération des informations,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/getRegisteredByUser/{email}/{tripId}/{tripDate}", produces = "application/json")
    public ResponseEntity getRegisteredByUser(@RequestHeader String token, @PathVariable(name = "email") String email, @PathVariable(name = "tripId") int tripId, @PathVariable(name = "tripDate") String tripDate) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.CLIENT);

        logger.debug("getRegisteredByUser " + email + " " + tripDate + " " + tripId);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        if (!email.equals(map.get("userEmail"))) {
            logger.warn("User " + map.get("userEmail") + " try to get registration of " + email);
            return ResponseEntity.badRequest().body("Client not authorized");
        }

        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(tripDate);
        } catch (ParseException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Bad date format");
        }

        List<Registration> registrations = registrationService.getRegisteredByUser(email, tripId, date);

        List<String> emails = new ArrayList<>();

        for (Registration registration : registrations) {
            emails.add(registration.getClient().getPerson().getEmail());
        }

        return ResponseEntity.ok(emails);
    }

    /**
     * Permet de récupérer les sortie qui n'ont pas eu lieu.
     *
     * @param token JWT token de l'utilisateur.
     * 200 avec un JSON contenant les informations des trip qui n'ont pas eu lieu,
     * 400 si une erreur s'est produite durant la récupération des données,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/purge", produces = "application/json")
    public ResponseEntity getPurge(@RequestHeader String token) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        logger.debug("getPurge");

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        List<TripList> tripList = tripService.getPurge();

        return ResponseEntity.ok(tripList);
    }

    /**
     * Permet de purger les sortie qui n'ont pas eu lieu.
     *
     * @param token JWT token de l'utilisateur.
     * 200 avec un JSON contenant les informations des trip purgés,
     * 400 si une erreur s'est produite durant la purge,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @PostMapping(value = "/purge", produces = "application/json")
    public ResponseEntity purge(@RequestHeader String token, @RequestBody List<TripList> tripList) {

        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        logger.debug("purge " + tripList);

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        try {
            tripService.purge(tripList);
            return ResponseEntity.ok("Purge success");

        } catch (DataIntegrityViolationException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("The action violates some constraints");
        } catch (NoSuchElementException e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Error during purge");
        } catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}