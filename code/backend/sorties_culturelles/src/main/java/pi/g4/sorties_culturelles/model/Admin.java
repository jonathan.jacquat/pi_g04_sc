package pi.g4.sorties_culturelles.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Admin  {

    @Id
    @Column(name =  "email", unique = true, nullable = true)
    private String email;

    @OneToOne
    @MapsId
    @JoinColumn(name = "email")
    private Person person;

    public Person getPerson() {
        return person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;

        if (person.getEmail() != null ? !person.getEmail().equals(admin.person.getEmail()) : admin.person.getEmail() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return person.getEmail() != null ? person.getEmail().hashCode() : 0;
    }
}
