package pi.g4.sorties_culturelles.model;

import java.util.Date;
import java.util.List;

public class RegistrationList {

    private List<String> clients;

    private int idTrip;

    private Date tripDate;

    private String registerer;

    public RegistrationList() {
    }

    public RegistrationList(List<String> clients, int idTrip, Date tripDate, String registerer) {
        this.clients = clients;
        this.idTrip = idTrip;
        this.tripDate = tripDate;
        this.registerer = registerer;
    }


    public List<String> getClients() {
        return clients;
    }

    public int getIdTrip() {
        return idTrip;
    }

    public Date getTripDate() {
        return tripDate;
    }

    public String getRegisterer() {
        return registerer;
    }
}
