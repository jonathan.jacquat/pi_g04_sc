package pi.g4.sorties_culturelles.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pi.g4.sorties_culturelles.model.Client;
import pi.g4.sorties_culturelles.model.Person;
import pi.g4.sorties_culturelles.model.UserLogin;
import pi.g4.sorties_culturelles.repository.ClientRepository;
import pi.g4.sorties_culturelles.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public UserLogin getUser(String email, String password) {
        Object[] objects = (Object[]) userRepository.getUser(email, password)[0];
        return new UserLogin((String) objects[0], (String) objects[1], (String) objects[2], (String) objects[3]);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Client> getAllUsers() {
        return clientRepository.findAll();
    }

}
