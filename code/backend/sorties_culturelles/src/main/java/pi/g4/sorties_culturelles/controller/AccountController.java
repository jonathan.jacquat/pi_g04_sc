package pi.g4.sorties_culturelles.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import pi.g4.sorties_culturelles.authorization.Role;
import pi.g4.sorties_culturelles.model.Client;
import pi.g4.sorties_culturelles.model.LoginInfos;
import pi.g4.sorties_culturelles.model.UserLogin;
import pi.g4.sorties_culturelles.service.AuthenticationService;
import pi.g4.sorties_culturelles.service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/account")
public class AccountController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    private final Logger logger = LoggerFactory.getLogger(AccountController.class);

    /**
     * Permet à un utilisateur de se connecter.
     *
     * @param loginInfos JSON contenant les informations de connexion de l'utilisateur
     * @return 200 avec un JSON contenant le token et les informations de l'utilisateur,
     * 401 si les informations de login ne sont pas corrects.
     */
    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity login(@RequestBody LoginInfos loginInfos) {
        UserLogin user = null;

        logger.debug("try login " + loginInfos.getEmail());

        try {
            user = userService.getUser(loginInfos.getEmail(), loginInfos.getPassword());
            if (user == null) {
                logger.warn("invalid login " + loginInfos.getEmail());
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid login");
            }
        } catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid login");
        }

        Jwt jwt = authenticationService.createNewJwt(user);

        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("token", jwt.getTokenValue());
        return ResponseEntity.ok(map);
    }

    /**
     * Permet de récupérer les informations des clients.
     *
     * @param token le token d'authentification de l'utilisateur.
     * @return 200 avec un JSON contenant les informations des clients,
     * 401 si le token est invalide ou l'utilisateur n'est pas autorisé.
     */
    @GetMapping(value = "/getUsers", produces = "application/json")
    public ResponseEntity getUsers(@RequestHeader String token) {
        Map<String, Object> map = authenticationService.validateToken(token, Role.ADMIN);

        logger.debug("getUsers");

        if (map.containsKey("responseEntity"))
            return (ResponseEntity) map.get("responseEntity");

        List<Client> clients = null;

        try {
            clients = userService.getAllUsers();
        } catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.badRequest().body("Error");
        }

        return ResponseEntity.ok(clients);
    }
}