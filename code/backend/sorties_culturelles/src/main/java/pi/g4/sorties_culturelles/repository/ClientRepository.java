package pi.g4.sorties_culturelles.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pi.g4.sorties_culturelles.model.Client;
import pi.g4.sorties_culturelles.model.Person;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Person>{

    @Query("SELECT c FROM Client c WHERE c.person.email = :email")
    Client findByEmail(String email);


}
