package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pi.g4.sorties_culturelles.model.IdentityDocument;

public interface IdentityDocumentRepository extends JpaRepository<IdentityDocument, String> {
}
