package pi.g4.sorties_culturelles.model;

public class LoginInfos {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
