package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pi.g4.sorties_culturelles.model.Address;
import pi.g4.sorties_culturelles.model.AddressPK;

public interface AddressRepository  extends JpaRepository<Address, AddressPK> {
}
