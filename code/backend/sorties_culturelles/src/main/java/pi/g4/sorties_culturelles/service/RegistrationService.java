package pi.g4.sorties_culturelles.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pi.g4.sorties_culturelles.model.*;
import pi.g4.sorties_culturelles.repository.ClientRepository;
import pi.g4.sorties_culturelles.repository.RegistrationRepository;
import pi.g4.sorties_culturelles.repository.TripRepository;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RegistrationService {
    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public List<Registration> register(RegistrationList registrations) throws Exception {

        Trip trip = null;
        TripDate tripDate = null;
        List<Client> clients = new ArrayList<>();
        Client registerer;

        try {
            trip = tripRepository.findById(registrations.getIdTrip()).get();
        } catch (NoSuchElementException e) {
            throw new Exception("Trip does not exist");
        }

        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        for (TripDate date : trip.getTripDate()) {
            if (fmt.format(date.getTripDate()).equals(fmt.format(registrations.getTripDate()))) {
                tripDate = date;
                break;
            }
        }
        if (tripDate == null) {
            throw new Exception("TripDate does not exist in this trip");
        }


        int nbRegistration = registrationRepository.getNbRegistration(registrations.getIdTrip(), registrations.getTripDate());
        int maxRegistration = tripRepository.findById(registrations.getIdTrip()).get().getMaxInscription();

        if (nbRegistration + registrations.getClients().size() > maxRegistration) {
            throw new Exception("Too many registration for this trip");
        }

        for (String clientMail : registrations.getClients()) {
            Client client = clientRepository.findByEmail(clientMail);
            if (client == null) {
                throw new Exception("Client " + clientMail + " is not in the database");
            } else {
                clients.add(client);
            }
        }

        registerer = clientRepository.findByEmail(registrations.getRegisterer());
        if (registerer == null) {
            throw new Exception("Registerer " + registrations.getRegisterer() + " is not in the database");
        }

        List<Registration> registrationList = new ArrayList<>();


        for (Client client : clients) {
            Registration registration = new Registration(client, trip, tripDate, registerer);
            registrationList.add(registrationRepository.save(registration));

        }

        return registrationList;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void unregister(RegistrationList registrations) throws Exception {

        for (String client : registrations.getClients()) {
            RegistrationPK registrationPK = new RegistrationPK(client, registrations.getIdTrip(), registrations.getTripDate());
            Registration registration = registrationRepository.getById(registrationPK);

            if (registration == null) {
                throw new Exception("Registration is not in the database");
            }
            registrationRepository.delete(registration);
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Registration> getRegistrationByTrip(int tripId, Date date) {

        List<Registration> registrations = new ArrayList<>();
        registrations.addAll(registrationRepository.getRegistrationByTrip(tripId, date));

        return registrations;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Registration> getRegistrationByEmail(String email) {

        List<Registration> registrations = new ArrayList<>();
        registrations.addAll(registrationRepository.getRegistrationByEmail(email));

        return registrations;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Registration> getRegisteredByUser(String email, int tripId, Date date) {

        List<Registration> registrations = new ArrayList<>();
        registrations.addAll(registrationRepository.getRegisteredByUser(email, tripId, date));

        return registrations;
    }
}