package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pi.g4.sorties_culturelles.model.TripDate;

import java.util.Date;

public interface TripDateRepository extends JpaRepository<TripDate, Date> {

    @Modifying
    @Query(nativeQuery = true, value ="CALL pr_remove_trip_trip_date(:idTrip, :tripDate)")
    void deleteTripTripDate(int idTrip, Date tripDate);
}
