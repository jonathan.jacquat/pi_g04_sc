package pi.g4.sorties_culturelles.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pi.g4.sorties_culturelles.model.*;
import pi.g4.sorties_culturelles.repository.*;

import java.util.*;

@Service
public class TripService {
    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private CategorieRepository categorieRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private IdentityDocumentRepository identityDocumentRepository;

    @Autowired
    private TripDateRepository tripDateRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Trip createTrip(Trip trip) {

        try {
            Location location = locationRepository.findById(trip.getAddress().getLocation().getPostcode()).get();
            trip.getAddress().setLocation(location);
        } catch (NoSuchElementException e) {}

        try {
            AddressPK addressPK = new AddressPK(trip.getAddress().getAddress(), trip.getAddress().getAddressNumber());
            Address address = addressRepository.findById(addressPK).get();
            trip.setAddress(address);
        } catch (NoSuchElementException e) {}

        try {
            Categorie categorie = categorieRepository.findById(trip.getCategorie().getName()).get();
            trip.setCategorie(categorie);
        } catch (NoSuchElementException e) {}


        HashSet<IdentityDocument> toRemoveTrip = new HashSet<>();
        HashSet<IdentityDocument> toAddTrip = new HashSet<>();

        for (IdentityDocument id : trip.getIdentityDocument()) {
            try {
                IdentityDocument idDB = identityDocumentRepository.findById(id.getName()).get();
                toRemoveTrip.add(id);
                toAddTrip.add(idDB);
            }catch (NoSuchElementException e) {}
        }
        trip.getIdentityDocument().removeAll(toRemoveTrip);
        trip.getIdentityDocument().addAll(toAddTrip);

        HashSet<TripDate> toRemoveDate = new HashSet<>();
        HashSet<TripDate> toAddDate = new HashSet<>();

        for (TripDate date : trip.getTripDate()) {
            try {
                TripDate dateDB = tripDateRepository.findById(date.getTripDate()).get();
                toRemoveDate.add(date);
                toAddDate.add(dateDB);
            }catch (NoSuchElementException e) {}
        }
        trip.getTripDate().removeAll(toRemoveDate);
        trip.getTripDate().addAll(toAddDate);

        return tripRepository.save(trip);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Trip updateTrip(Trip trip) throws Exception {
        for (TripDate date : trip.getTripDate()) {
            if (registrationRepository.getNbRegistration(trip.getIdTrip(), date.getTripDate()) > 0) {
                throw new Exception("A trip with some registrations cannot be updated");
            }
        }

        try {
            Location location = locationRepository.findById(trip.getAddress().getLocation().getPostcode()).get();
            trip.getAddress().setLocation(location);
        } catch (NoSuchElementException e) {
            locationRepository.save(trip.getAddress().getLocation());
        }

        try {
            AddressPK addressPK = new AddressPK(trip.getAddress().getAddress(), trip.getAddress().getAddressNumber());
            Address address = addressRepository.findById(addressPK).get();
            trip.setAddress(address);
        } catch (NoSuchElementException e) {
            addressRepository.save(trip.getAddress());
        }

        try {
            Categorie categorie = categorieRepository.findById(trip.getCategorie().getName()).get();
            trip.setCategorie(categorie);
        } catch (NoSuchElementException e) {
            categorieRepository.save(trip.getCategorie());
        }

        HashSet<IdentityDocument> toRemoveTrip = new HashSet<>();
        HashSet<IdentityDocument> toAddTrip = new HashSet<>();

        for (IdentityDocument id : trip.getIdentityDocument()) {
            try {
                IdentityDocument idDB = identityDocumentRepository.findById(id.getName()).get();
                toRemoveTrip.add(id);
                toAddTrip.add(idDB);
            }catch (NoSuchElementException e) {
                identityDocumentRepository.save(id);
            }
        }
        trip.getIdentityDocument().removeAll(toRemoveTrip);
        trip.getIdentityDocument().addAll(toAddTrip);

        HashSet<TripDate> toRemoveDate = new HashSet<>();
        HashSet<TripDate> toAddDate = new HashSet<>();

        for (TripDate date : trip.getTripDate()) {
            try {
                TripDate dateDB = tripDateRepository.findById(date.getTripDate()).get();
                toRemoveDate.add(date);
                toAddDate.add(dateDB);
            }catch (NoSuchElementException e) {
                tripDateRepository.save(date);
            }
        }
        trip.getTripDate().removeAll(toRemoveDate);
        trip.getTripDate().addAll(toAddDate);

        return tripRepository.save(trip);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Trip getTrip(int tripId) {
        try{
            return tripRepository.findById(tripId).get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Trip> getAllTrips() {
        List<Trip> trips = tripRepository.findAll();
        ArrayList<Trip> futur_trips = new ArrayList<>();
        Date today = new Date();


        for (Trip trip : trips) {
            for (TripDate tripDate : trip.getTripDate()) {
                if (tripDate.getTripDate().compareTo(today) > 0) {
                    futur_trips.add(trip);
                    break;
                }
            }
        }
        return futur_trips;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<TripList> getTripsValidate() {
        List<Trip> trips = tripRepository.findAll();
        List<TripList> validate_trips = new ArrayList<>();
        Date today = new Date();


        for (Trip trip : trips) {
            for (TripDate tripDate : trip.getTripDate()) {
                if (registrationRepository.getNbRegistration(trip.getIdTrip(), tripDate.getTripDate()) >= trip.getMinInscription()){
                    validate_trips.add(new TripList(trip.getIdTrip(), trip.getTitle(), tripDate.getTripDate()));
                }
            }
        }
        return validate_trips;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<TripList> getPurge() {
        List<Trip> trips = tripRepository.findAll();
        List<TripList> purge_trips = new ArrayList<>();
        Date today = new Date();


        for (Trip trip : trips) {
            for (TripDate tripDate : trip.getTripDate()) {
                if (tripDate.getTripDate().compareTo(today) <= 0) {
                    if (registrationRepository.getNbRegistration(trip.getIdTrip(), tripDate.getTripDate()) < trip.getMinInscription()){
                        purge_trips.add(new TripList(trip.getIdTrip(), trip.getTitle(), tripDate.getTripDate()));
                    }
                }
            }
        }
        return purge_trips;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void purge(List<TripList> tripLists) {

        for (TripList purge : tripLists) {
            Trip trip = tripRepository.findById(purge.getIdTrip()).get();

            tripDateRepository.deleteTripTripDate(purge.getIdTrip(), purge.getTripDate());

            List<Registration> registrations = registrationRepository.getRegistrationByTrip(purge.getIdTrip(), purge.getTripDate());
            for (Registration registration : registrations) {
                registrationRepository.delete(registration);
            }

            int tripSize = trip.getTripDate().size();

            if (tripSize == 0) {
                tripRepository.delete(trip);
            }
        }
    }
}
