package pi.g4.sorties_culturelles.model;

import javax.persistence.*;

@Entity
public class Location {
    @Id
    @Column(name = "postcode", nullable = false)
    private int postcode;
    @Basic
    @Column(name = "country", nullable = false, length = 20)
    private String country;
    @Basic
    @Column(name = "city", nullable = false, length = 20)
    private String city;

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (postcode != location.postcode) return false;
        if (country != null ? !country.equals(location.country) : location.country != null) return false;
        if (city != null ? !city.equals(location.city) : location.city != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = postcode;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }
}
