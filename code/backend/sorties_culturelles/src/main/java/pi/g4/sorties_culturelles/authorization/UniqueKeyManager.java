package pi.g4.sorties_culturelles.authorization;

import org.springframework.security.crypto.keys.KeyManager;
import org.springframework.security.crypto.keys.ManagedKey;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class UniqueKeyManager implements KeyManager {

    private final ManagedKey managedKey;

    public UniqueKeyManager(KeyPair keyPair) {
        this(keyPair.getPublic(), keyPair.getPrivate());
    }

    public UniqueKeyManager(PublicKey publicKey, PrivateKey privateKey) {
        this(ManagedKey
                .withAsymmetricKey(publicKey, privateKey)
                .keyId(UUID.randomUUID().toString())
                .activatedOn(Instant.now())
                .build());
    }

    public UniqueKeyManager(ManagedKey managedKey) {
        this.managedKey = managedKey;
    }

    @Override
    public ManagedKey findByKeyId(String keyId) {
        if (keyId.equals(this.managedKey.getKeyId())) {
            return this.managedKey;
        }
        return null;
    }

    @Override
    public Set<ManagedKey> findByAlgorithm(String algorithm) {
        if (algorithm.equals(this.managedKey.getAlgorithm())) {
            return set(this.managedKey);
        }
        return null;
    }

    @Override
    public Set<ManagedKey> getKeys() {
        return set(this.managedKey);
    }

    private static <T> Set<T> set(T object) {
        Set<T> set = new HashSet<>();
        set.add(object);
        return set;
    }
}
