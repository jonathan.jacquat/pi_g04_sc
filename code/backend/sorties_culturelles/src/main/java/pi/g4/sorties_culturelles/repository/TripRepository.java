package pi.g4.sorties_culturelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pi.g4.sorties_culturelles.model.Trip;

public interface TripRepository extends JpaRepository<Trip, Integer> {

}
