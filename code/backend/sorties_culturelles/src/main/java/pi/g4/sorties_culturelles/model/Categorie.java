package pi.g4.sorties_culturelles.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Categorie {
    @Id
    @Column(name = "name", nullable = false, length = 20)
    private String name;

    @OneToMany(mappedBy = "categorie")
    private List<Trip> trips;

    public Categorie(String name) {
        this.name = name;
    }

    public Categorie() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Categorie categorie = (Categorie) o;

        if (name != null ? !name.equals(categorie.name) : categorie.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
