package pi.g4.sorties_culturelles.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "trip_date", schema = "public", catalog = "sc_db")
public class TripDate {
    @Id
    @Column(name = "trip_date", nullable = false)
    private Date tripDate;

    public Date getTripDate() {
        return tripDate;
    }

    public void setTripDate(Date tripDate) {
        this.tripDate = tripDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TripDate tripDate1 = (TripDate) o;

        if (tripDate != null ? !tripDate.equals(tripDate1.tripDate) : tripDate1.tripDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return tripDate != null ? tripDate.hashCode() : 0;
    }
}