package pi.g4.sorties_culturelles.model;

import javax.persistence.*;

@Entity
@IdClass(AddressPK.class)
public class Address {
    @Id
    @Column(name = "address", nullable = false, length = 50)
    private String address;

    @Id
    @Column(name = "address_number", nullable = false)
    private int addressNumber;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "postcode", referencedColumnName="postcode")
    private Location location;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(int addressNumber) {
        this.addressNumber = addressNumber;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        if (addressNumber != address1.addressNumber) return false;
        if (location != address1.location) return false;
        if (address != null ? !address.equals(address1.address) : address1.address != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + addressNumber;
        result = 31 * result + location.getPostcode();
        return result;
    }
}
