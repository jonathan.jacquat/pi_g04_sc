package pi.g4.sorties_culturelles.model;

public class UserLogin extends Person {

    private String role;

    public UserLogin(String email, String firstName, String lastName, String role) {
        super(email, firstName, lastName);
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
