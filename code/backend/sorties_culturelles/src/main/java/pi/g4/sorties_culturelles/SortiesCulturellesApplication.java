package pi.g4.sorties_culturelles;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.jose.jws.NimbusJwsEncoder;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.jwt.*;
import pi.g4.sorties_culturelles.authorization.KeyReader;
import pi.g4.sorties_culturelles.authorization.UniqueKeyManager;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@SpringBootApplication(
		exclude = {SecurityAutoConfiguration.class}
)

@Configuration
public class SortiesCulturellesApplication {

	private RSAPublicKey jwtPublicKey;
	private RSAPrivateKey jwtPrivateKey;

	public SortiesCulturellesApplication() {
		KeyReader keyReader = new KeyReader();
		try {
			jwtPublicKey = keyReader.readPublicKey("data/public-key.pem");
			jwtPrivateKey = keyReader.readPrivateKey("data/private-key-pkcs8.pem");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(SortiesCulturellesApplication.class, args);
	}

	@Bean
	public JwtDecoder newJwtDecoder() {
		return NimbusJwtDecoder
				.withPublicKey(this.jwtPublicKey)
				.signatureAlgorithm(SignatureAlgorithm.RS256)
				.build();
	}

	@Bean
	public JwtEncoder newJwtEncoder() {
		return new NimbusJwsEncoder(new UniqueKeyManager(this.jwtPublicKey, this.jwtPrivateKey));
	}

}
