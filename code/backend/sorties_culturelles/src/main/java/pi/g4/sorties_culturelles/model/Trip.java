package pi.g4.sorties_culturelles.model;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Set;

@Entity
public class Trip {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_trip", nullable = false)
    private int idTrip;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "categorie", referencedColumnName="name")
    private Categorie categorie;


    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumns({
            @JoinColumn(name="address", referencedColumnName="address"),
            @JoinColumn(name="address_number", referencedColumnName="address_number")
    })
    private Address address;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "requirement", joinColumns = @JoinColumn(name = "id_trip", referencedColumnName = "id_trip"), inverseJoinColumns = @JoinColumn(name = "identity_document", referencedColumnName = "name"))
    private Set<IdentityDocument> identityDocument;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "trip_trip_date", joinColumns = @JoinColumn(name = "id_trip", referencedColumnName = "id_trip"), inverseJoinColumns = @JoinColumn(name = "trip_date", referencedColumnName = "trip_date"))
    private Set<TripDate> tripDate;

    @Basic
    @Column(name = "title", nullable = false, length = 50)
    private String title;

    @Basic
    @Column(name = "max_inscription", nullable = false)
    private int maxInscription;
    @Basic
    @Column(name = "min_inscription", nullable = false)
    private int minInscription;
    @Basic
    @Column(name = "responsible_name", nullable = false, length = 50)
    private String responsibleName;
    @Basic
    @Column(name = "responsible_surname", nullable = false, length = 50)
    private String responsibleSurname;
    @Basic
    @Column(name = "price", nullable = false)
    private int price;
    @Basic
    @Column(name = "description_xml", nullable = false, length = -1)
    private String descriptionXml;
    @Basic
    @Column(name = "image", nullable = true)
    private byte[] image;
    @Basic
    @Column(name = "video", nullable = true, length = 50)
    private String video;
    @Basic
    @Column(name = "version", nullable = false)
    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getIdTrip() {
        return idTrip;
    }

    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Address getAddress() {
        return address;
    }

    public Set<IdentityDocument> getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(Set<IdentityDocument> identityDocument) {
        this.identityDocument = identityDocument;
    }

    public Set<TripDate> getTripDate() {
        return tripDate;
    }

    public void setTripDate(Set<TripDate> tripDate) {
        this.tripDate = tripDate;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxInscription() {
        return maxInscription;
    }

    public void setMaxInscription(int maxInscription) {
        this.maxInscription = maxInscription;
    }

    public int getMinInscription() {
        return minInscription;
    }

    public void setMinInscription(int minInscription) {
        this.minInscription = minInscription;
    }

    public String getResponsibleName() {
        return responsibleName;
    }

    public void setResponsibleName(String responsibleName) {
        this.responsibleName = responsibleName;
    }

    public String getResponsibleSurname() {
        return responsibleSurname;
    }

    public void setResponsibleSurname(String responsibleSurname) {
        this.responsibleSurname = responsibleSurname;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescriptionXml() {
        return descriptionXml;
    }

    public void setDescriptionXml(String descriptionXml) {
        this.descriptionXml = descriptionXml;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trip trip = (Trip) o;

        if (idTrip != trip.idTrip) return false;
        if (maxInscription != trip.maxInscription) return false;
        if (minInscription != trip.minInscription) return false;
        if (categorie != null ? !categorie.equals(trip.categorie) : trip.categorie != null) return false;
        if (address != null ? !address.equals(trip.address) : trip.address != null) return false;
        if (title != null ? !title.equals(trip.title) : trip.title != null) return false;
        if (responsibleName != null ? !responsibleName.equals(trip.responsibleName) : trip.responsibleName != null)
            return false;
        if (responsibleSurname != null ? !responsibleSurname.equals(trip.responsibleSurname) : trip.responsibleSurname != null)
            return false;
        if (price != trip.price) return false;
        if (descriptionXml != null ? !descriptionXml.equals(trip.descriptionXml) : trip.descriptionXml != null)
            return false;
        if (!Arrays.equals(image, trip.image)) return false;
        if (video != null ? !video.equals(trip.video) : trip.video != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTrip;
        result = 31 * result + (categorie != null ? categorie.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + maxInscription;
        result = 31 * result + minInscription;
        result = 31 * result + (responsibleName != null ? responsibleName.hashCode() : 0);
        result = 31 * result + (responsibleSurname != null ? responsibleSurname.hashCode() : 0);
        result = 31 * result + price;
        result = 31 * result + (descriptionXml != null ? descriptionXml.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + (video != null ? video.hashCode() : 0);
        return result;
    }
}
