package pi.g4.sorties_culturelles.model;

import java.util.Date;

public class TripList {

    private int idTrip;

    private String tripName;

    private Date tripDate;

    public TripList() {
    }

    public TripList(int idTrip, String tripName, Date tripDate) {
        this.idTrip = idTrip;
        this.tripName = tripName;
        this.tripDate = tripDate;
    }

    public int getIdTrip() {
        return idTrip;
    }

    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public Date getTripDate() {
        return tripDate;
    }

    public void setTripDate(Date tripDate) {
        this.tripDate = tripDate;
    }
}
